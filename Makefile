BUILD = build --no-cache
BUILD_PATH = .

all: build

build:
	docker $(BUILD) -t pyfreebilling/pyfb-router2 -f Dockerfile $(BUILD_PATH)

.PHONY: build
