
import os

from sqlalchemy import MetaData, create_engine

from databases import Database

DATABASE_URL = os.getenv("DATABASE_URL")

# SQLAlchemy
engine = create_engine(DATABASE_URL)
metadata = MetaData()


# databases query builder
database = Database(DATABASE_URL)
