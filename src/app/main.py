from datadog import initialize, statsd
from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from loguru import logger
from starlette.exceptions import HTTPException
from starlette_exporter import handle_metrics, PrometheusMiddleware
from timing_asgi import TimingMiddleware, TimingClient
from timing_asgi.integrations import StarletteScopeToName

from app.core.config import API_PREFIX, DEBUG, PROJECT_NAME, VERSION
from app.core.events import create_start_app_handler, create_stop_app_handler
from app.api.errors.http_error import http_error_handler
from app.api.errors.validation_error import http422_error_handler
from app.api.routes.api import router as api_router


initialize({'api_key': '8d1abb1d9f7014a96233fe5c8f0bc423', 'app_key': '66a1c5d2522c0dfd1c8136f97a54a32e8503e188'})


class StatsdClient(TimingClient):
    def __init__(self, datadog_client, tags=None):
        if tags is None:
            tags = []
        self.tags = tags
        self.datadog_client = datadog_client

    def timing(self, metric_name, timing, tags):
        logger.debug(f"timing: {metric_name} - {timing * 1000:>8.3f}ms - {tags + self.tags}")
        #self.datadog_client.timing(metric_name, timing, tags + self.tags)


def get_application() -> FastAPI:

    application = FastAPI(title=PROJECT_NAME, debug=DEBUG, version=VERSION)

    application.add_event_handler("startup", create_start_app_handler(application))
    application.add_event_handler("shutdown", create_stop_app_handler(application))

    application.add_exception_handler(HTTPException, http_error_handler)
    application.add_exception_handler(RequestValidationError, http422_error_handler)

    application.include_router(api_router, prefix=API_PREFIX)

    application.add_middleware(
        TimingMiddleware,
        client=StatsdClient(
            datadog_client=statsd,
            tags=['app_version:1.0.0'],
        ),
        metric_namer=StarletteScopeToName(
            prefix="pyfb-router2-dev",
            starlette_app=application
        )
    )
    application.add_middleware(PrometheusMiddleware, group_paths=True)
    application.add_route("/metrics/", handle_metrics)


    return application


app = get_application()
