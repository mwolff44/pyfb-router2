from app.services import rate as rate_info
from app.schemas.rate import RateSchema
from app.db.repositories.direction import DirectionRepository
from app.db.repositories.rate import RateRepository
from app.db.repositories.ratecard import RatecardRepository
from app.api.dependencies.database import get_repository

from fastapi import APIRouter, Depends, HTTPException, Path

router = APIRouter()


@router.get("/customer_ratecard/{ratecard_id}/callee/{callee_number}/callee_id/{callee_destination_id}", response_model=RateSchema)
async def read_rate(
    ratecard_id: int = Path(..., gt=0),
    callee_number: str = Path(...),
    callee_destination_id: int = Path(..., gt=0),
    rate_repo: RateRepository = Depends(get_repository(RateRepository)),
):

    rate = {}
    return await rate_info.get(
        rate_repo,
        ratecard_id,
        callee_number,
        callee_destination_id
    )

    if not rate:
        raise HTTPException(status_code=404, detail="Customer rate not found")

    return rate


@router.get("/customer/{customer_id}/callee/{callee_number}/caller_id/{caller_destination_id}", response_model=RateSchema)
async def read_rate(
    customer_id: int = Path(..., gt=0),
    callee_number: str = Path(...),
    caller_destination_id: int = Path(..., gt=0),
    direction_repo: DirectionRepository = Depends(get_repository(DirectionRepository)),
    rate_repo: RateRepository = Depends(get_repository(RateRepository)),
    ratecard_repo: RatecardRepository = Depends(get_repository(RatecardRepository)),
):

    rate = {}
    return await rate_info.get_with_customer_id(
        direction_repo,
        rate_repo,
        ratecard_repo,
        customer_id,
        callee_number,
        caller_destination_id
    )

    if not rate:
        raise HTTPException(status_code=404, detail="Customer rate not found")

    return rate
