from typing import List

from app.services import balance as balance_status
from app.schemas.balance import BalanceSchema
from app.db.repositories.balance import BalanceRepository
from app.api.dependencies.database import get_repository

from fastapi import APIRouter, Depends, HTTPException, Path

router = APIRouter()


@router.get("/{id}/", response_model=BalanceSchema, name="balance:get")
async def read_balance(
    id: int = Path(..., gt=0),
    balance_repo: BalanceRepository = Depends(get_repository(BalanceRepository)),
):
    """
    """
    balance_info = {}
    balance_info = await balance_status.get(balance_repo, id)
    
    if not balance_info:
        raise HTTPException(status_code=404, detail="Customer not found")
    
    return balance_info