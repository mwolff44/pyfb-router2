from fastapi import APIRouter

router = APIRouter()


@router.get("/ping", name="status:ping")
async def pong():
    # some async operation could happen here
    return {"ping": "pong!"}
