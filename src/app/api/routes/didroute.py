from loguru import logger
from typing import List

from app.services import direction as direction_detail
from app.services import normalization
from app.services import rate as rate_info
from app.services import ratecard as ratecard_status
from app.services import route as route_status
from app.services import common
from app.schemas.route import RouteSchema, RouteJsonSchema
from app.db.repositories.didroute import DidRouteRepository
from app.db.repositories.direction import DirectionRepository
from app.db.repositories.normalization import NormalizationRepository
from app.db.repositories.rate import RateRepository
from app.db.repositories.ratecard import RatecardRepository
from app.db.repositories.route import RouteRepository
from app.api.dependencies.database import get_repository

from fastapi import APIRouter, Depends, HTTPException, Path

router = APIRouter()


@router.get(
    "/inbound/caller_number/{caller_number}/callee_number/{callee_number}/socket_in/{socket}/socket_out/{socket_out}",
    response_model=dict  # RouteJsonSchema
)
async def read_route(
    caller_number: str = Path(...),
    callee_number: str = Path(...),
    socket: str = Path(...),
    socket_out: str = Path(...),
    didroute_repo: DidRouteRepository = Depends(get_repository(DidRouteRepository)),
    direction_repo: DirectionRepository = Depends(get_repository(DirectionRepository)),
    normalization_repo: NormalizationRepository = Depends(get_repository(NormalizationRepository)),
    rate_repo: RateRepository = Depends(get_repository(RateRepository)),
    ratecard_repo: RatecardRepository = Depends(get_repository(RatecardRepository)),
    route_repo: RouteRepository = Depends(get_repository(RouteRepository)),
):
    """
    """

    # get route list
    route_info = []
    route_info = await route_status.get_did(
        didroute_repo,
        callee_number
    )

    if not route_info:
        error = {
            "status_code": 404,
            "detail": "Route not found"
        }
        route_rtjson = (
            {
                "success": False,
                "error": error,
            }
        )
        rtjson = {
            "routing": route_rtjson,
        }
        logger.debug(f"rtjson : {rtjson}")

        return rtjson

    # get direction_id from caller number
    caller_destination = {}
    caller_destination = await direction_detail.get(
        direction_repo, caller_number
    )
    logger.debug(f"caller_number {caller_number} / id : {caller_destination}")
    if caller_destination is False:
        raise HTTPException(
            status_code=404,
            detail="Caller destination not found"
        )
    else:
        caller_destination_id = caller_destination["destination_id"]

    # get direction_id from callee number
    callee_destination = {}
    callee_destination = await direction_detail.get(
        direction_repo, callee_number
    )
    logger.debug(f"callee_number {callee_number} / id : {callee_destination}")
    if callee_destination is False:
        raise HTTPException(
            status_code=404,
            detail="Callee destination not found"
        )
    else:
        callee_destination_id = callee_destination["destination_id"]

    callee_number_without_prefix = callee_number
    did_destination_id = callee_destination_id

    # get provider rate by route TO BE MODIFIED
    prov_rate = {}

    route_dict = [dict(r) for r in route_info]

    logger.debug(f"route dict detail : {route_dict}")

    routes = []
    # Handles the creation of route for all available routes and gateways
    for r in range(len(route_dict)):
        # Handle CallerID rules

        if route_dict[r]["dst_type"] == 's':
            if route_dict[r]["registration"] is True:
                if route_dict[r]["contact"] is None:
                    logger.info(f"contact {route_dict[r]['name']} is not registered !")
                    continue
                else:
                    logger.debug(f"contact {route_dict[r]['name']} is registered !")
                    _, r_user, r_uri_ip, r_uri_port, r_params = common.split_uri_to_parts(route_dict[r]["contact"])
                    dst_uri = f"{route_dict[r]['received']}"
            else:
                r_uri_ip, _ = route_dict[r]["sip_ip"].split('/')
                r_uri_port = route_dict[r]['sip_port']
                dst_uri = f"sip:{r_uri_ip}:{route_dict[r]['sip_port']}"
        elif route_dict[r]["dst_type"] == 'e':
            logger.debug(f"Outbound call forward !")
            routes_out = []
            try:
                routes_out, rate_out, caller_destination_id, callee_destination_id, callee_number_without_prefix = await route_status.get_outbound_route(
                    direction_repo,
                    normalization_repo,
                    rate_repo,
                    ratecard_repo,
                    route_repo,
                    route_dict[r]['customer_id'],
                    caller_number,
                    route_dict[r]['dst_number'],
                    socket_out,
                )
            except HTTPException as e:
                error = {
                    "status_code": e.status_code,
                    "detail": e.detail
                }
                route_rtjson = (
                    {
                        "success": False,
                        "error": error,
                    }
                )
                rtjson = {
                    "routing": route_rtjson,
                    "rating": ""
                }
                logger.debug(f"rtjson : {rtjson}")

                return rtjson

            routes = routes + routes_out
            continue
        else:
            logger.debug(f"unknown routing rule => ignore !")
            continue

        if route_dict[r]['callerid_norm_in_id']:
            caller_number_norm = await normalization.get(
                normalization_repo,
                route_dict[r]['callerid_norm_in_id'],
                caller_number
            )
            logger.debug(f"Caller number - {caller_number_norm} - has been normalized by rule id {route_dict[r]['callerid_norm_in_id']}")
        else:
            caller_number_norm = caller_number
        new_caller_id, headers_extra = common.set_caller_id(
            caller_number_norm,
            route_dict[r]["pai"],
            route_dict[r]["ppi"],
            route_dict[r]["pid"],
            False,
            socket
        )

        # if username add Organization header
        if r_user:
            headers_extra = headers_extra + "Organization: " + r_user + "\r\n"

        logger.debug(f"new callerID : {new_caller_id}")
        logger.debug(f"extra headers : {headers_extra}")

        # Handle Callee rules
        if route_dict[r]['callee_norm_in_id']:
            new_callee_id = await normalization.get(
                normalization_repo,
                route_dict[r]['callee_norm_in_id'],
                callee_number
            )
            logger.debug(f"called Did number - {new_callee_id} - has been normalized by rule id {route_dict[r]['callee_norm_in_id']}")
        else:
            new_callee_id = callee_number

        logger.debug(f"new calleeID : {new_callee_id}")

        # Contruct routes structures
        to_header = {
            "display": new_callee_id,
            "uri": f"sip:{new_callee_id}@{r_uri_ip}"
        }
        logger.debug(f"To header : {to_header}")

        from_header = common.construct_from_header(
            True,
            new_caller_id,
            socket,
            ''
        )
        logger.debug(f"From header : {from_header}")

        # dst_uri = f"sip:{route_dict[r]['received']}"
        logger.debug(f"dst_uri : {dst_uri}")
        if r_params:
            uri = f"sip:{new_callee_id}@{r_uri_ip}:{r_uri_port};{r_params}"
        else:
            f"sip:{new_callee_id}@{r_uri_ip}:{r_uri_port}"
        logger.debug(f"uri : {uri}")

        routes.append(
            {
                "uri": uri,
                "dst_uri": dst_uri,
                "path": "",
                "socket": socket,
                "headers": {
                    "from": from_header,
                    "to": to_header,
                    "extra": headers_extra,
                },
                "branch_flags": 8,
                "fr_timer": 5000,
                "fr_inv_timer": 30000,
                "call_class_leg_b": "on-net"
            }
        )

    # build the JSON document, compatible with the rtjson Kamailio module
    route_rtjson = (
        {
            "success": True,
            "version": "1.0",
            "routing": "serial",
            "routes": routes,
        }
        if routes
        else {"success": False}
    )

    rtjson = {
        "routing": route_rtjson,
        "rating": {
            "terminator_cust": {
                "cust_id": f"{route_dict[0]['customer_id']}",
                "ratecard_id": "0",
                "rate": "0",
                "rate_type": "0",
                "rate_id": "0"
            },
            "originator_prov": {
                "prov_id": f"{route_dict[0]['provider_id']}",
                "ratecard_id": "0",
                "rate": "0",
                "rate_type": "0",
                "rate_id": "0"
            },
        },
        "phonenumbers": {
            "did": callee_number,
            "did_direction": f"{did_destination_id}",
            "called_number": callee_number_without_prefix,
            "caller_number": caller_number,
            "called_direction": f"{callee_destination_id}",
            "caller_direction": f"{caller_destination_id}"
        }
    }

    logger.debug(f"rtjson : {rtjson}")

    return rtjson
