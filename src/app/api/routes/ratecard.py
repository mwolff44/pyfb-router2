from typing import List

from app.services import ratecard as ratecard_status
from app.schemas.ratecard import RatecardSchema
from app.db.repositories.ratecard import RatecardRepository
from app.api.dependencies.database import get_repository

from fastapi import APIRouter, Depends, HTTPException, Path

router = APIRouter()


@router.get(
    "/customer/{customer_id}/caller/{caller_destination_id}",
    response_model=List[RatecardSchema]
)
async def read_ratecard(
    customer_id: int = Path(..., gt=0),
    caller_destination_id: int = Path(..., gt=0),
    ratecard_repo: RatecardRepository = Depends(get_repository(RatecardRepository)),
):
    """
    """
    ratecard_info = []
    ratecard_info = await ratecard_status.get(ratecard_repo, customer_id, caller_destination_id)
    
    if not ratecard_info:
        raise HTTPException(status_code=404, detail="Customer ratecard not found")
    
    return ratecard_info