from fastapi import APIRouter

from app.api.routes import balance, didroute, direction, normalization, rate, ratecard, route, status

router = APIRouter()

router.include_router(status.router)
router.include_router(balance.router, prefix="/balance", tags=["balance"])
# router.include_router(company.router, prefix="/company", tags=["company"])
# router.include_router(customer.router, prefix="/customer", tags=["customer"])
router.include_router(didroute.router, prefix="/route", tags=["route"])
router.include_router(direction.router, prefix="/direction", tags=["direction"])
router.include_router(normalization.router, prefix="/normalization", tags=["normalization"])
router.include_router(ratecard.router, prefix="/ratecard", tags=["ratecard"])
router.include_router(rate.router, prefix="/rate", tags=["rate"])
router.include_router(route.router, prefix="/route", tags=["route"])