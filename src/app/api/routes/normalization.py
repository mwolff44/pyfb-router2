from app.services import normalization as normalization_status
from app.schemas.normalization import NormalizationSchema
from app.db.repositories.normalization import NormalizationRepository
from app.api.dependencies.database import get_repository

from fastapi import APIRouter, Depends, HTTPException, Path

router = APIRouter()


@router.get(
    "/normgroup/{normgroup_id}/phonenumber/{phonenumber}",
    response_model=str
)
async def normalize_number(
    normgroup_id: int = Path(..., gt=0),
    phonenumber: str = Path(...),
    normalization_repo: NormalizationRepository = Depends(get_repository(NormalizationRepository)),
):
    """provides a normalized phone number regarding group rules
    """
    normalization_info = []
    normalization_info = await normalization_status.get(normalization_repo, normgroup_id, phonenumber)
    
    if not normalization_info:
        raise HTTPException(status_code=404, detail="Normalization  group not found")
    
    return normalization_info


""" @router.get(
    "/customer_endpoint/{endpoint_id}/caller_number/{caller_number}/called_number/{called_number}/direction/{in_out}/",
    response_model=str
)
async def customer_normalization(
    normgroup_id: int = Path(..., gt=0),
    phonenumber: str = Path(...),
    normalization_repo: NormalizationRepository = Depends(get_repository(NormalizationRepository)),
):
    provides normalized phone number (caller and callee number) depending on customer endpoint 
    and call direction form endpoint point of view
    
    normalization_info = []
    normalization_info = await normalization_status.get(normalization_repo, normgroup_id, phonenumber)
    
    if not normalization_info:
        raise HTTPException(status_code=404, detail="Normalization  group not found")
    
    return normalization_info """


""" @router.get(
    "/provider_endpoint/{endpoint_id}/caller_number/{caller_number}/called_number/{called_number}/direction/{in_out}/",
    response_model=str
)
async def provider_normalization(
    normgroup_id: int = Path(..., gt=0),
    phonenumber: str = Path(...),
    normalization_repo: NormalizationRepository = Depends(get_repository(NormalizationRepository)),
):
    provides normalized phone number (caller and callee number) depending on provider endpoint 
    and call direction form endpoint point of view
    
    normalization_info = []
    normalization_info = await normalization_status.get(normalization_repo, normgroup_id, phonenumber)
    
    if not normalization_info:
        raise HTTPException(status_code=404, detail="Normalization  group not found")
    
    return normalization_info """