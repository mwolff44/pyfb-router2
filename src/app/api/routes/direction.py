from typing import List

from app.services import direction as direction_detail
from app.schemas.direction import DirectionSchema
from app.db.repositories.direction import DirectionRepository
from app.api.dependencies.database import get_repository

from fastapi import APIRouter, Depends, HTTPException, Path

router = APIRouter()


@router.get("/{phonenumber}/", response_model=DirectionSchema)
async def read_direction(
    phonenumber: str,
    direction_repo: DirectionRepository = Depends(get_repository(DirectionRepository)),
):
    
    direction_info = {}
    direction_info = await direction_detail.get(direction_repo, phonenumber)
    
    if not direction_info:
        raise HTTPException(status_code=404, detail="Direction not found")
    
    return direction_info