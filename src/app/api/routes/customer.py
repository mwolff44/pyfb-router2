from typing import List

from app.services import customer
from app.schemas.company import CustomerSchema, CustomerDB
from fastapi import APIRouter, HTTPException, Path

router = APIRouter()


@router.post("/", response_model=CustomerDB, status_code=201)
async def create_customer(payload: CustomerSchema):
    customer_id = await customer.post(payload)

    response_object = {
        "id": customer_id,
        "credit_limit": payload.credit_limit,
        "low_credit_alert": payload.low_credit_alert,
        "max_calls": payload.max_calls,
        "calls_per_second": payload.calls_per_second,
        "customer_enabled": payload.customer_enabled,
        "company_id": payload.company_id
    }
    return response_object


@router.get("/{id}/", response_model=CustomerDB)
async def read_customer(id: int = Path(..., gt=0),):
    r_customer = await customer.get(id)
    if not r_customer:
        raise HTTPException(status_code=404, detail="customer not found")
    return r_customer


@router.get("/", response_model=List[CustomerDB])
async def read_all_companies():
    return await customer.get_all()


@router.put("/{id}/", response_model=CustomerDB)
async def update_note(payload: CustomerSchema, id: int = Path(..., gt=0),):
    u_customer = await customer.get(id)
    if not u_customer:
        raise HTTPException(status_code=404, detail="customer not found")

    customer_id = await customer.put(id, payload)

    response_object = {
        "id": customer_id,
        "credit_limit": payload.credit_limit,
        "low_credit_alert": payload.low_credit_alert,
        "max_calls": payload.max_calls,
        "calls_per_second": payload.calls_per_second,
        "customer_enabled": payload.customer_enabled,
        "company_id": payload.company_id
    }
    return response_object


@router.delete("/{id}/", response_model=CustomerDB)
async def delete_customer(id: int = Path(..., gt=0)):
    d_customer = await customer.get(id)
    if not d_customer:
        raise HTTPException(status_code=404, detail="customer not found")

    await customer.delete(id)

    return d_customer