from loguru import logger
from typing import List

from app.services import balance as balance_status
from app.services import route as route_status
from app.schemas.balance import BalanceSchema
from app.schemas.route import RouteSchema, RouteJsonSchema
from app.db.errors import EntityDoesNotExist
from app.db.repositories.balance import BalanceRepository
from app.db.repositories.direction import DirectionRepository
from app.db.repositories.normalization import NormalizationRepository
from app.db.repositories.rate import RateRepository
from app.db.repositories.ratecard import RatecardRepository
from app.db.repositories.route import RouteRepository
from app.api.dependencies.database import get_repository

from fastapi import APIRouter, Depends, HTTPException, Path

router = APIRouter()


@router.get(
    "/outbound/customer_id/{customer_id}/caller_number/{caller_number}/callee_number/{callee_number}/socket/{socket}",
    response_model=dict  # RouteJsonSchema
)
async def read_route(
    customer_id: int = Path(..., gt=0),
    caller_number: str = Path(...),
    callee_number: str = Path(...),
    socket: str = Path(...),
    balance_repo: BalanceRepository = Depends(get_repository(BalanceRepository)),
    direction_repo: DirectionRepository = Depends(get_repository(DirectionRepository)),
    normalization_repo: NormalizationRepository = Depends(get_repository(NormalizationRepository)),
    rate_repo: RateRepository = Depends(get_repository(RateRepository)),
    ratecard_repo: RatecardRepository = Depends(get_repository(RatecardRepository)),
    route_repo: RouteRepository = Depends(get_repository(RouteRepository)),
):
    """
    """

    # Get customer info and balance status
    balance_info = {}
    balance_info = await balance_status.get(balance_repo, customer_id)
    
    if not balance_info or balance_info["customer_status"] is False:
        error = {
            "status_code": 403,
            "detail": "No Customer found"
        }
        route_rtjson = (
            {
                "success": False,
                "error": error,
            }
        )
        rtjson = {
            "routing": route_rtjson,
        }
        return rtjson
    
    if balance_info["balance_status"] is False and balance_info["blocking_credit_limit"] is True:
        error = {
            "status_code": 503,
            "detail": "Low balance"
        }
        route_rtjson = (
            {
                "success": False,
                "error": error,
            }
        )
        rtjson = {
            "routing": route_rtjson,
        }
        return rtjson

    routes, rate = {}, {}
    # Get route list
    try:
        routes, rate, caller_destination_id, callee_destination_id, callee_number_without_prefix = await route_status.get_outbound_route(
            direction_repo,
            normalization_repo,
            rate_repo,
            ratecard_repo,
            route_repo,
            customer_id,
            caller_number,
            callee_number,
            socket,
        )
    except HTTPException as e:
        error = {
            "status_code": e.status_code,
            "detail": e.detail
        }

    # build the JSON document, compatible with the rtjson Kamailio module
    if routes:
        route_rtjson = (
            {
                "success": True,
                "version": "1.0",
                "routing": "serial",
                "routes": routes,
            }
        )
        rtjson = {
            "routing": route_rtjson,
            "rating": {
                "originator_cust": {
                    "cust_id": customer_id,
                    "ratecard_id": f"{rate['ratecard_id']}",
                    "rate": f"{rate['rate']}",
                    "rate_type": f"{rate['rate_type']}",
                    "rate_id": f"{rate['rate_id']}"
                },
            },
            "phonenumbers": {
                "did": "0",
                "called_number": callee_number_without_prefix,
                "caller_number": caller_number,
                "called_direction": f"{callee_destination_id}",
                "caller_direction": f"{caller_destination_id}"
            }
        }
    else:
        route_rtjson = (
            {
                "success": False,
                "error": error,
            }
        )
        rtjson = {
            "routing": route_rtjson,
        }

    logger.debug(f"rtjson : {rtjson}")

    return rtjson


@router.get(
    "/outbound/customer_id/{customer_id}/caller_number/{caller_number}/{caller_destination_id}/callee_number/{callee_number}/{callee_destination_id}/socket/{socket}",
    response_model=List[RouteSchema]
)
async def read_route(
    customer_id: int = Path(..., gt=0),
    caller_number: str = Path(...),
    callee_number: str = Path(...),
    caller_destination_id: int = Path(..., gt=0),
    callee_destination_id: int = Path(..., gt=0),
    socket: str = Path(...),
    route_repo: RouteRepository = Depends(get_repository(RouteRepository)),
):
    """
    """

    # get route list
    route_info = []
    route_info = await route_status.get(
        route_repo,
        customer_id,
        callee_number,
        callee_destination_id,
        socket,
    )
    
    if not route_info:
        raise HTTPException(status_code=404, detail="Route not found")
    
    return route_info