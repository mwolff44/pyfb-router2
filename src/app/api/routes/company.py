from typing import List

from app.services import company
from app.schemas.company import CompanySchema, CompanyDB
from fastapi import APIRouter, HTTPException, Path

router = APIRouter()


@router.post("/", response_model=CompanyDB, status_code=201)
async def create_company(payload: CompanySchema):
    company_id = await company.post(payload)

    response_object = {
        "id": company_id,
        "name": payload.name,
        "customer_balance": payload.customer_balance,
        "supplier_balance": payload.supplier_balance,
    }
    return response_object


@router.get("/{id}/", response_model=CompanyDB)
async def read_company(id: int = Path(..., gt=0),):
    r_company = await company.get(id)
    if not r_company:
        raise HTTPException(status_code=404, detail="company not found")
    return r_company


@router.get("/", response_model=List[CompanyDB])
async def read_all_companies():
    return await company.get_all()


@router.put("/{id}/", response_model=CompanyDB)
async def update_note(payload: CompanySchema, id: int = Path(..., gt=0),):
    u_company = await company.get(id)
    if not u_company:
        raise HTTPException(status_code=404, detail="company not found")

    company_id = await company.put(id, payload)

    response_object = {
        "id": company_id,
        "name": payload.name,
        "customer_balance": payload.customer_balance,
        "supplier_balance": payload.supplier_balance
    }
    return response_object


@router.delete("/{id}/", response_model=CompanyDB)
async def delete_company(id: int = Path(..., gt=0)):
    d_company = await company.get(id)
    if not d_company:
        raise HTTPException(status_code=404, detail="company not found")

    await company.delete(id)

    return d_company
