from pydantic import BaseModel


class NormalizationSchema(BaseModel):
    profile: str
    rule: str
    pr: int
    match_len: int
    match_op: int
    match_exp: str
    subst_exp: str
    repl_exp: str
