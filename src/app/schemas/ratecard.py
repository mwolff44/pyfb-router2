from pydantic import BaseModel, Field, PositiveInt


class RatecardSchema(BaseModel):
    tech_prefix: str
    priority: int
    discount: float
    allow_negative_margin: bool
    ratecard_id: int
