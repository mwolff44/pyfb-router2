from pydantic import BaseModel, Field, PositiveInt


class DirectionSchema(BaseModel):
    prefix_id: PositiveInt
    destination_id: PositiveInt
    destination_name: str