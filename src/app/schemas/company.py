from pydantic import BaseModel, condecimal, Field, PositiveInt


class CompanySchema(BaseModel):
    name: str = Field(..., min_length=3, max_length=200, example="Celea Consulting")
    customer_balance: condecimal(max_digits=12, decimal_places=6) = 0
    supplier_balance: condecimal(max_digits=12, decimal_places=6) = 0


class CompanyDB(CompanySchema):
    id: int


class CustomerSchema(BaseModel):
    credit_limit: condecimal(max_digits=12, decimal_places=4) = 0
    low_credit_alert: condecimal(max_digits=12, decimal_places=4) = 10
    blocking_credit_limit: bool = True
    max_calls: PositiveInt = 1
    calls_per_second: PositiveInt = 1
    customer_enabled: bool = True
    company_id: PositiveInt = Field(...)


class CustomerDB(CustomerSchema):
    id: int
