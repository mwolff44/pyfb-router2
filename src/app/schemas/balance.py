from pydantic import BaseModel, Field


class BalanceSchema(BaseModel):
    balance_status: bool
    customer_status: bool
    blocking_credit_limit: bool


class BalanceDB(BalanceSchema):
    customer_id: int
