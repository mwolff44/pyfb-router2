from pydantic import BaseModel, Field, PositiveInt
from typing import List


class RouteSchema(BaseModel):
    "Route handle routing information"
    id: int
    destnum_length_map: int
    route_type: int
    providerendpoint_id: int
    provider_ratecard_id: int
    route_rule: str
    status: str
    weight: int
    priority: int
    prefix: str = None
    destnum_length: int = None
    destination_id: int = None
    country_id: int = None
    type_id: int = None
    region_id: int = None
    name: str
    callee_norm_id: int = None
    callee_norm_in_id: int = None
    caller_id_in_from: bool
    callerid_norm_id: int = None
    callerid_norm_in_id: int = None
    from_domain: str
    pai: bool
    ppi: bool
    pid: bool
    add_plus_in_caller: bool
    gwprefix: str
    sip_transport: str
    sip_port: int
    sip_proxy: str
    username: str
    gwregister: bool
    password: str = None
    realm: str = None
    suffix: str
    estimated_quality: int
    ratecard_prefix: str
    provider_id: int
    provider_rate: int = None


class DidRouteSchema(BaseModel):
    "Route handle DID routing information"
    did: str
    customer_id: int
    provider_id: int
    route_type: int
    order: int
    dst_type: str
    dst_number: str = None
    weight: int
    trunk_id: int = None
    name: str = None
    pai: bool = None
    ppi: bool = None
    pid: bool = None
    callee_norm_in_id: int = None
    callerid_norm_in_id: int = None
    contact: str = None
    received: str = None
    path: str = None
    q: float = None
    expires: str = None  # datetime with timezone
    registration: bool = None
    sip_ip: str = None
    sip_port: int = None
    sip_transport: str = None


class HeaderDetailSchema(BaseModel):
    "HeaderDetail contains Display and uri fields"
    display: str
    uri: str


class HeaderSchema(BaseModel):
    "Header contains from and to headers dat"
    extra: str
    from_h: HeaderDetailSchema
    to: HeaderDetailSchema


class RouteDetailSchema(BaseModel):
    "RouteDetail contains all data of one route"
    branch_flags: int
    dst_uri: str
    fr_inv_timer: int
    fr_timer: int
    headers: HeaderSchema
    path: str
    socket: str
    uri: str


class RouteJsonSchema(BaseModel):
    "RouteJson contains all data for routing"
    success: bool
    routes: str
    routing: str
    version: str