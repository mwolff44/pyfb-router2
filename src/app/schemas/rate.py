from pydantic import BaseModel, Field, PositiveInt


class RateSchema(BaseModel):
    id: int
    destnum_length_map: int
    ratecard_id: int
    rate_type: int
    ratecard_name: str
    rc_type: str
    status: str
    rate: int
    block_min_duration: int
    minimal_time: int
    init_block: int
    prefix: str = None
    destnum_length: int = None
    destination_id: int = None
    country_id: int = None
    type_id: int = None
    region_id: int = None
    rate_id: int
