import re

from loguru import logger

from typing import Tuple


re_protocol_local_part_and_domain = re.compile(
    r'^([^:]+:)?([^@]+)@([^@:]+):([0-9]+.*)?$'
).match

# sip:user:password@host:port;uri-parameters?headers
uriPattern = re.compile(
    '(?P<scheme>\w+):' #Scheme
    +'(?:(?P<user>[\w\.]+):?(?P<password>[\w\.]+)?@)?' #User:Password
    +'\[?(?P<host>' #Begin group host
        +'(?:\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})|' #IPv4 address Host Or
        +'(?:(?:[0-9a-fA-F]{1,4}):){7}[0-9a-fA-F]{1,4}|' #IPv6 address Host Or
        +'(?:(?:[0-9A-Za-z]+\.)+[0-9A-Za-z]+)'#Hostname string
    +')\]?:?' #End group host
    +'(?P<port>\d{1,6})?' #port
    +'(?:\;(?P<params>[^\?]*))?' # parameters
    +'(?:\?(?P<headers>.*))?' # headers
)

groupNamesList = ['scheme', 'user', 'password', 'host', 'port', 'params', 'headers'] #List of group Names

def set_caller_id(
    caller_id: str,
    pai: bool,
    ppi: bool,
    pid: bool,
    add_plus_in_caller: bool,
    socket: str
): #-> str, str
    "SetCallerID sets the callerId and corresponding headers regarding the GW rules"
    
    # Set caller_id regarding GW rules
    extra_headers = ""

    # Add + in front of CallerID for outbound call
    if add_plus_in_caller is True:
        caller_id = "+" + caller_id
    
    # If anonymous
    #If PAI, set it
    # RFC 3325
    if pai is True:
        extra_headers = extra_headers + "P-Asserted-Identity: <sip:" + caller_id + "@" + socket + ">\r\n"
    

    # If PPI, set it
    # RFC 3325
    if ppi is True:
        extra_headers = extra_headers + "P-Preferred-Identity: <sip:" + caller_id + "@" + socket + ">\r\n"
    

    # If RPID, set it
    # https://tools.ietf.org/html/draft-ietf-sip-privacy-00
    if pid is True:
        extra_headers = extra_headers + "Remote-Party-ID: <sip:" + caller_id + "@" + socket + ">\r\n"

    return caller_id, extra_headers


def set_callee(
    callee_id: str,
    gwprefix: str,
    suffix: str,
    ratecard_prefix: str
): #-> str
    """SetCallee sets the CalleeId regarding the GW rules - it concatenates gateway prefix with
        ratecard prefix with callee_id and at the end the suffix
    """
    # Set Callee regarding GW rules

    callee_id = gwprefix + ratecard_prefix + callee_id + suffix

    return callee_id


def construct_from_header(
    callerid_in_from: bool,
    new_caller_id: str,
    socket: str,
    username: str
): #-> HeaderDetailSchema
    "ConstructFromHeader constructs the SIP FROM header"

    if callerid_in_from == True:
        #from_header = HeaderDetail{Display: new_caller_id, URI: "sip:" + new_caller_id + "@" + socket}
        from_header = {
            "display": new_caller_id,
            "uri": "sip:" + new_caller_id + "@" + socket,
        }
    else:
        #from_header = HeaderDetail{Display: "", URI: "sip:" + username + "@" + socket}
        from_header = {
            "display": "",
            "uri": "sip:" + username + "@" + socket,
        }

    return from_header


def split_uri_to_parts(uri: str) -> Tuple[str, str, str, str]:
    mObject = uriPattern.search(uri) #pattern search
    if mObject: #if you find a match
        groupStrings = [mObject.group(groupName) if mObject.group(groupName) else '' for groupName in groupNamesList] #extract your groupStrings
        logger.debug(f"SIP uri split : {groupStrings}")
        ## print('Scheme: {0}, User: {1}, Password: {2}, Host: {3}, Port: {4}, Params: {5}, Headers: {6}'.format(*groupStrings)) #print groupStrings
    else:
        return ('', '', '', '', '')
    scheme, user, password, host, port, params, headers = mObject.groups()
    return (scheme or '', user, host, port or '5060', params)
