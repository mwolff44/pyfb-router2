from loguru import logger

from app.db.errors import EntityDoesNotExist
from app.api.dependencies.database import get_repository
from app.db.repositories.ratecard import RatecardRepository


async def get(ratecard_repo: RatecardRepository, customer_id: int, caller_destination_id: int):
    
    ratecard = {}

    try:
        ratecard = await ratecard_repo.get_ratecard_list_by_customer_id(customer_id=customer_id, caller_destination_id=caller_destination_id)
    except EntityDoesNotExist:
        return False
   
    logger.debug(f"ratecard: {ratecard}")
    return ratecard
