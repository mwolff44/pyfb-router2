from loguru import logger

from app.db.errors import EntityDoesNotExist
from app.api.dependencies.database import get_repository
from app.db.repositories.balance import BalanceRepository
from app.schemas.balance import BalanceSchema


async def get(balance_repo: BalanceRepository, id: int):

    balance = {}

    try:
        balance = await balance_repo.get_balance_status_by_customer_id(id=id)
    except EntityDoesNotExist:
        return False

    logger.debug("balance: {0}", balance)
    return balance
