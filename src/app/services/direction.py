from loguru import logger

from app.db.errors import EntityDoesNotExist
from app.api.dependencies.database import get_repository
from app.db.repositories.direction import DirectionRepository
from app.schemas.direction import DirectionSchema


async def get(direction_repo: DirectionRepository, phonenumber: str):

    direction = {}

    try:
        direction = await direction_repo.get_direction_by_call_number(phonenumber=phonenumber)
    except EntityDoesNotExist:
        return False
    
    logger.debug("direction: {0}", direction)
    return direction