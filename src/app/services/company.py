from app.schemas.company import CompanySchema



async def post(payload: CompanySchema):
    query = t_company.insert().values(
        name=payload.name,
        customer_balance=payload.customer_balance,
        supplier_balance=payload.supplier_balance
    )
    return await database.execute(query=query)


async def get(id: int):
    query = t_company.select().where(id == t_company.c.id)
    return await database.fetch_one(query=query)


async def get_all():
    query = t_company.select()
    return await database.fetch_all(query=query)


async def put(id: int, payload: CompanySchema):
    query = (
        t_company
        .update()
        .where(id == t_company.c.id)
        .values(
            name=payload.name,
            customer_balance=payload.customer_balance,
            supplier_balance=payload.supplier_balance
        )
        .returning(t_company.c.id)
    )
    return await database.execute(query=query)


async def delete(id: int):
    query = t_company.delete().where(id == t_company.c.id)
    return await database.execute(query=query)
