from loguru import logger

from fastapi import HTTPException

from app.db.errors import EntityDoesNotExist
from app.api.dependencies.database import get_repository
from app.db.repositories.didroute import DidRouteRepository
from app.db.repositories.direction import DirectionRepository
from app.db.repositories.normalization import NormalizationRepository
from app.db.repositories.rate import RateRepository
from app.db.repositories.ratecard import RatecardRepository
from app.db.repositories.route import RouteRepository
from app.services import direction as direction_detail
from app.services import normalization
from app.services import rate as rate_info
from app.services import ratecard as ratecard_status
from app.services import route as route_status
from app.services import common


async def get(
    route_repo: RouteRepository,
    customer_id: int,
    callee_number: str,
    callee_destination_id: int,
    socket: str
):

    route = {}

    try:
        route = await route_repo.get_outbound_route_list_by_customer_id(
            customer_id=customer_id,
            callee_number=callee_number,
            callee_destination_id=callee_destination_id
        )
    except EntityDoesNotExist:
        return False

    logger.debug(f"route: {route}")
    return route


async def get_did(
    didroute_repo: DidRouteRepository,
    callee_number: str
):

    route = {}

    try:
        route = await didroute_repo.get_did_route_list_by_did(
            did=callee_number
        )
    except EntityDoesNotExist:
        return False

    logger.debug(f"route: {route}")
    return route


async def get_outbound_route(
    direction_repo: DirectionRepository,
    normalization_repo: NormalizationRepository,
    rate_repo: RateRepository,
    ratecard_repo: RatecardRepository,
    route_repo: RouteRepository,
    customer_id: int,
    caller_number: str,
    callee_number: str,
    socket: str,
):
    """Outbound routes"""

    # get direction_id from caller number
    caller_destination = {}
    caller_destination = await direction_detail.get(
        direction_repo,
        caller_number
    )
    logger.debug(f"caller_number {caller_number} / id : {caller_destination}")
    if caller_destination is False:
        raise HTTPException(
            status_code=404,
            detail="Caller destination not found"
        )
    else:
        caller_destination_id = caller_destination["destination_id"]



    # get customer rate
    rate = {}
    rate = await rate_info.get_with_customer_id(
        direction_repo,
        rate_repo,
        ratecard_repo,
        customer_id,
        callee_number,
        caller_destination_id
    )

    if rate is None:
        raise HTTPException(status_code=404, detail="Customer rate not found")

    # Get new Called number
    callee_number_without_prefix = rate["callee_number_without_prefix"]

    # get direction_id from callee number
    callee_destination = {}
    callee_destination = await direction_detail.get(
        direction_repo,
        callee_number_without_prefix
    )
    logger.debug(f"callee_number {callee_number_without_prefix} / id : {callee_destination}")
    if callee_destination is False:
        raise HTTPException(
            status_code=404,
            detail="Callee destination not found"
        )
    else:
        callee_destination_id = callee_destination["destination_id"]

    # get route list
    route_info = []
    route_info = await route_status.get(
        route_repo,
        customer_id,
        callee_number_without_prefix,
        callee_destination_id,
        socket,
    )

    if not route_info:
        raise HTTPException(status_code=404, detail="Route not found")

    # get provider rate by route TO BE MODIFIED
    prov_rate = {}
    prov_rate_list = []

    route_dict = [dict(r) for r in route_info]

    logger.debug(f"route dict detail : {route_dict[0]['username']}")

    for k in range(len(route_dict)):
        logger.debug(f"ratecard record detail : {route_dict[k]}")
        ratecard_id = route_dict[k]["provider_ratecard_id"]
        prov_rate = await rate_info.get_provrate(
            rate_repo,
            ratecard_id,
            callee_number_without_prefix,
            callee_destination_id
        )
        if prov_rate:
            logger.debug(f"provider rate detail : {prov_rate['rate']}")
            route_dict[k]['provider_rate'] = prov_rate["rate"]
            route_dict[k]['rate_type'] = prov_rate["rate_type"]
            route_dict[k]['rate_id'] = prov_rate["rate_id"]
            prov_rate_list.append(prov_rate)
        else:
            continue

    if prov_rate is None:
        raise HTTPException(status_code=404, detail="Provider rate not found")

    logger.debug(f"route dict detail : {route_dict}")

    # Ordering routes depending on routing rule
    logger.debug(f"route ordering rule : {route_dict[0]['route_rule']}")

    if route_dict[0]["route_rule"] == "LCR":
        route_dict.sort(key=lambda route_dict: route_dict["provider_rate"])
    elif route_dict[0]["route_rule"] == "PRIO":
        route_dict.sort(key=lambda route_dict: route_dict["priority"])
    elif route_dict[0]["route_rule"] == "QUALITY":
        route_dict.sort(key=lambda route_dict: route_dict["estimated_quality"])
    elif route_dict[0]["route_rule"] == "WEIGHT":
        # // Calculate total weight
        # var totalWeight int
        # for _, c := range custRoutes:
        #    totalWeight += c.Weight
        logger.debug(f"ordered routes type WEIGHT not working")

        # ToDo
        # example of algo : https://medium.com/@peterkellyonline/weighted-random-selection-3ff222917eb6
        # for the moment, weight is not taken, only fair distribution

    logger.debug(f"ordered routes : {route_dict}")

    routes = []
    # Handles the creation of route for all available routes and gateways
    for r in range(len(route_dict)):
        # Handle CallerID rules
        if route_dict[r]['callerid_norm_in_id']:
            caller_number_norm = await normalization.get(
                normalization_repo,
                route_dict[r]['callerid_norm_in_id'],
                caller_number
            )
            logger.debug(f"Caller number - {caller_number_norm} - has been normalized by rule id {route_dict[r]['callerid_norm_in_id']}")
        else:
            caller_number_norm = caller_number
        new_caller_id, headers_extra = common.set_caller_id(
            caller_number_norm,
            route_dict[r]["pai"],
            route_dict[r]["ppi"],
            route_dict[r]["pid"],
            route_dict[r]["add_plus_in_caller"],
            socket
        )
        logger.debug(f"new callerID : {new_caller_id}")
        logger.debug(f"extra headers : {headers_extra}")

        # Handle Callee rules
        if route_dict[r]['callee_norm_in_id']:
            callee_number_norm = await normalization.get(
                normalization_repo,
                route_dict[r]['callee_norm_in_id'],
                callee_number_without_prefix
            )
            logger.debug(f"called number - {callee_number_norm} - has been normalized by rule id {route_dict[r]['callee_norm_in_id']}")
        else:
            callee_number_norm = callee_number_without_prefix
        new_callee_id = common.set_callee(
            callee_number_norm,
            route_dict[r]["gwprefix"],
            route_dict[r]["suffix"],
            route_dict[r]["ratecard_prefix"]
        )
        logger.debug(f"new calleeID : {callee_number_norm}")

        # Contruct routes structures
        to_header = {
            "display": new_callee_id,
            "uri": f"sip:{new_callee_id}@{route_dict[r]['sip_proxy']}:{route_dict[r]['sip_port']}"
        }
        logger.debug(f"To header : {to_header}")

        from_header = common.construct_from_header(
            route_dict[r]["caller_id_in_from"],
            new_caller_id,
            socket,
            route_dict[r]["username"]
        )
        logger.debug(f"From header : {from_header}")

        dst_uri = f"sip:{route_dict[r]['sip_proxy']}:{route_dict[r]['sip_port']}"
        logger.debug(f"dst_uri : {dst_uri}")
        uri = f"sip:{new_callee_id}@{route_dict[r]['sip_proxy']}:{route_dict[r]['sip_port']}"
        logger.debug(f"uri : {uri}")

        routes.append(
            {
                "uri": uri,
                "dst_uri": dst_uri,
                "path": "",
                "socket": socket,
                "headers": {
                    "from": from_header,
                    "to": to_header,
                    "extra": headers_extra,
                },
                "branch_flags": 8,
                "fr_timer": 5000,
                "fr_inv_timer": 30000,
                "call_class_leg_b": "off-net",
                "terminator_provider": f"{route_dict[r]['provider_id']}",
                "terminator_p_rate": f"{route_dict[r]['provider_rate']}",
                "rate_type": f"{route_dict[r]['rate_type']}",
                "rate_id": f"{route_dict[r]['rate_id']}"
            }
        )

    logger.debug(f"route list: {routes}")
    return routes, rate, caller_destination_id, callee_destination_id, callee_number_without_prefix
