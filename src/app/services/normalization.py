import re

from fastapi import HTTPException

from loguru import logger

from app.db.errors import EntityDoesNotExist
from app.api.dependencies.database import get_repository
from app.db.repositories.normalization import NormalizationRepository


# remove from string unwanted caracter for a phonenumber - only digit is allowed
clean_number = re.compile('[^0-9]').sub


async def get(
    normalization_repo: NormalizationRepository,
    group_id: int,
    phonenumber: str
) -> str:
    "return a normalized phone number formatted according to group_id rules"
    
    normalization = {}
    phonenumber = clean_number('', phonenumber)

    try:
        normalization = await normalization_repo.get_norm_rule_list_by_group_id(group_id=group_id)
    except EntityDoesNotExist:
        return False
    
    rules_dict = [dict(r) for r in normalization]
    logger.debug(f"rules list for profile {group_id} : {rules_dict}")

    norm_number = normalize(phonenumber, rules_dict)
   
    logger.debug(f"normalized number: {norm_number}")
    return norm_number



def normalize(phonenumber: str, rules: dict) -> str:
    "return a phone number normalized according to rules"

    number = phonenumber
    
    for k in range(len(rules)):

        logger.debug(f"rule to be evaluated : {rules[k]}")

        if rules[k]["match_op"] != 1:
          raise HTTPException(status_code=404, detail="Rule error")

        if (
                (
                    rules[k]['match_len'] > 0 and len(phonenumber) == rules[k]['match_len']
                )
                or rules[k]['match_len'] == 0
        ):
            if re.fullmatch(rules[k]['match_exp'], phonenumber):
                number = re.sub(rules[k]['subst_exp'], rules[k]['repl_exp'], phonenumber)
                return number
            else:
                continue         
        else:
            continue

    return number