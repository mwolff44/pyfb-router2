from app.schemas.company import CustomerSchema



async def post(payload: CustomerSchema):
    query = t_customer.insert().values(
        credit_limit=payload.credit_limit,
        low_credit_alert=payload.low_credit_alert,
        max_calls=payload.max_calls,
        calls_per_second=payload.calls_per_second,
        customer_enabled=payload.customer_enabled,
        company_id=payload.company_id
    )
    return await database.execute(query=query)


async def get(id: int):
    query = t_customer.select().where(id == t_customer.c.id)
    return await database.fetch_one(query=query)


async def get_all():
    query = t_customer.select()
    return await database.fetch_all(query=query)


async def put(id: int, payload: CustomerSchema):
    query = (
        t_customer
        .update()
        .where(id == t_customer.c.id)
        .values(
            credit_limit=payload.credit_limit,
            low_credit_alert=payload.low_credit_alert,
            max_calls=payload.max_calls,
            calls_per_second=payload.calls_per_second,
            customer_enabled=payload.customer_enabled,
            company_id=payload.company_id
        )
        .returning(t_customer.c.id)
    )
    return await database.execute(query=query)


async def delete(id: int):
    query = t_customer.delete().where(id == t_customer.c.id)
    return await database.execute(query=query)