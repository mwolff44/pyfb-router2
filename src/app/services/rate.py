from loguru import logger

from fastapi import HTTPException

from app.db.errors import EntityDoesNotExist
from app.api.dependencies.database import get_repository
from app.db.repositories.direction import DirectionRepository
from app.db.repositories.rate import RateRepository
from app.db.repositories.ratecard import RatecardRepository
from app.schemas.rate import RateSchema
from app.services import direction as direction_detail
from app.services import ratecard as ratecard_status


async def get(
    rate_repo: RateRepository,
    ratecard_id: int,
    callee_number: str,
    callee_destination_id: int
):
    """return customer rate from ratecard_id"""
    rate = {}

    try:
        rate = await rate_repo.get_cust_rate_by_ratecard_id(
            ratecard_id=ratecard_id,
            callee_number=callee_number,
            callee_destination_id=callee_destination_id
        )
    except EntityDoesNotExist:
        return False

    logger.debug(f"rate: {rate}")
    return rate


async def get_with_customer_id(
    direction_repo: DirectionRepository,
    rate_repo: RateRepository,
    ratecard_repo: RatecardRepository,
    customer_id: int,
    callee_number: str,
    caller_destination_id: int
):
    """return rate from customer_id"""
    # get ratecard list
    ratecard_info = []
    ratecard_info = await ratecard_status.get(
        ratecard_repo,
        customer_id,
        caller_destination_id
    )

    if not ratecard_info:
        raise HTTPException(status_code=404, detail="Customer ratecard not found")
    else:
        logger.debug(f"ratecard list length : {len(ratecard_info)}")

    # get customer rate
    rate = {}

    for record in ratecard_info:
        logger.debug(f"ratecard record detail : {dict(record)}")
        ratecard_id = dict(record)["ratecard_id"]
        callee_number_without_prefix = callee_number.strip(dict(record)["tech_prefix"])
        # get direction_id from callee number
        callee_destination = {}
        callee_destination = await direction_detail.get(
            direction_repo,
            callee_number_without_prefix
        )
        logger.debug(f"callee_number {callee_number_without_prefix} / id : {callee_destination}")
        if callee_destination is False:
            raise HTTPException(
                status_code=404,
                detail="Callee destination not found"
            )
        else:
            callee_destination_id = callee_destination["destination_id"]
        logger.debug(f"original callee number : {callee_number} - callee number without tech prefix: {callee_number_without_prefix}")
        rate = await get(
            rate_repo,
            ratecard_id,
            callee_number_without_prefix,
            callee_destination_id
        )
        if rate:
            break
        else:
            continue

    if rate is None:
        raise HTTPException(status_code=404, detail="Customer rate not found")
    rate_dict = dict(rate)

    # add called number with tech prefix
    rate_dict["callee_number_without_prefix"] = callee_number_without_prefix

    logger.debug(f"rate: {rate_dict}")
    return rate_dict


async def get_provrate(rate_repo: RateRepository, ratecard_id: int, callee_number: str, callee_destination_id: int):

    provrate = {}

    try:
        provrate = await rate_repo.get_prov_rate_by_ratecard_id(
            ratecard_id=ratecard_id,
            callee_number=callee_number,
            callee_destination_id=callee_destination_id
        )
    except EntityDoesNotExist:
        return False

    logger.debug(f"provider rate: {provrate}")
    return provrate
