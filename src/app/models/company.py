from sqlalchemy import Column, DateTime, Integer, Numeric, Boolean, ForeignKey, String, Table, CheckConstraint
from sqlalchemy.sql import func

from app.db import metadata


t_company = Table(
    'pyfb_company', metadata,
    Column('id', Integer, primary_key=True),
    Column('name', String(200), nullable=False, unique=True),
    Column('customer_balance', Numeric(12, 6), default=0.0),
    Column('supplier_balance', Numeric(12, 6), default=0.0)
)


t_customer = Table(
    'pyfb_customer', metadata,
    Column('id', Integer, primary_key=True),
    #Column('created', DateTime(True), nullable=False),
    #Column('modified', DateTime(True), nullable=False),
    #Column('account_number', String(100), nullable=False),
    Column('credit_limit', Numeric(12, 4), default=0),
    Column('low_credit_alert', Numeric(12, 4), default=10),
    Column('max_calls', Integer, default=1),
    Column('calls_per_second', Integer, default=10),
    Column('customer_enabled', Boolean, default=True),
    Column('company_id', ForeignKey('pyfb_company.id', deferrable=True, initially='DEFERRED'), nullable=False, unique=True),
    CheckConstraint('calls_per_second >= 0'),
    CheckConstraint('max_calls >= 0')
)