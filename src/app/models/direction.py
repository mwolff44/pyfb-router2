from sqlalchemy import Column, DateTime, Integer, Numeric, Boolean, ForeignKey, String, Table, CheckConstraint
from sqlalchemy.sql import func

from app.db import metadata


t_direction_destination = Table(
    'pyfb_direction_destination', metadata,
    Column('id', Integer, primary_key=True),
    Column('name', String(255), nullable=False, unique=True)
)

""" t_direction_destination = Table(
    'pyfb_direction_destination', metadata,
    Column('id', Integer, primary_key=True, server_default=text("nextval('pyfb_direction_destination_id_seq'::regclass)")),
    Column('created', DateTime(True), nullable=False),
    Column('modified', DateTime(True), nullable=False),
    Column('name', String(255), nullable=False, unique=True),
    Column('slug', String(50), nullable=False, index=True),
    Column('carrier_id', ForeignKey('pyfb_direction_carrier.id', deferrable=True, initially='DEFERRED'), index=True),
    Column('country_iso2_id', ForeignKey('pyfb_direction_country.country_iso2', deferrable=True, initially='DEFERRED'), index=True),
    Column('type_id', ForeignKey('pyfb_direction_type.id', deferrable=True, initially='DEFERRED'), nullable=False, index=True)
) """


t_direction_prefix = Table(
    'pyfb_direction_prefix', metadata,
    Column('id', Integer, primary_key=True),
    Column('prefix', String(15), nullable=False, unique=True),
    Column('destination_id', ForeignKey('pyfb_direction_destination.id', deferrable=True, initially='DEFERRED'), nullable=False, index=True)
)
