from sqlalchemy import Column, DateTime, ForeignKey, Index, Integer, Table, Text, String, UniqueConstraint

from app.db import metadata


t_rating_c_ratecard = Table(
    'pyfb_rating_c_ratecard', metadata,
    Column('id', Integer, primary_key=True),
    Column('created', DateTime(True), nullable=False),
    Column('modified', DateTime(True), nullable=False),
    Column('status', String(100), nullable=False),
    Column('status_changed', DateTime(True), nullable=False),
    Column('description', Text, nullable=False),
    Column('date_start', DateTime(True), nullable=False),
    Column('date_end', DateTime(True), nullable=False),
    Column('name', String(128), nullable=False, unique=True),
    Column('slug', String(50), nullable=False, index=True),
    Column('rc_type', String(10), nullable=False),
    Column('callerid_list_id', ForeignKey('pyfb_rating_callernum_list.id', deferrable=True, initially='DEFERRED'), index=True),
    Index('pyfb_rating_id_2b3593_partial', 'id', 'rc_type'),
    Index('pyfb_rating_id_83b1f8_partial', 'id', 'rc_type'),
    Index('pyfb_rating_id_8d344b_partial', 'id', 'rc_type'),
    Index('pyfb_rating_id_7ee8d2_partial', 'id', 'rc_type'),
    Index('pyfb_rating_id_f993d1_partial', 'id', 'rc_type')
)


t_rating_callernum_list_destination = Table(
    'pyfb_rating_callernum_list_destination', metadata,
    Column('id', Integer, primary_key=True),
    Column('callernumlist_id', ForeignKey('pyfb_rating_callernum_list.id', deferrable=True, initially='DEFERRED'), nullable=False, index=True),
    Column('destination_id', ForeignKey('pyfb_direction_destination.id', deferrable=True, initially='DEFERRED'), nullable=False, index=True),
    UniqueConstraint('callernumlist_id', 'destination_id')
)


t_rating_callernum_list = Table(
    'pyfb_rating_callernum_list', metadata,
    Column('id', Integer, primary_key=True),
    Column('created', DateTime(True), nullable=False),
    Column('modified', DateTime(True), nullable=False),
    Column('name', String(128), nullable=False, unique=True),
    Column('slug', String(50), nullable=False, index=True),
    Column('callerid_filter', String(2), nullable=False)
)