import time

from typing import List, Optional, Sequence

from asyncpg import Connection, Record

from loguru import logger

from app.db.errors import EntityDoesNotExist
from app.db.repositories.base import BaseRepository
from app.schemas.route import RouteSchema


GET_OUTBOUND_ROUTE_LIST_BY_CUSTOMER_ID_QUERY = """
with r as (
        SELECT row_number() OVER () AS id,
            v.destnum_length_map,
            v.route_type,
            v.providerendpoint_id,
            v.provider_ratecard_id,
            v.route_rule,
            v.status,
            v.weight,
            v.priority,
            v.prefix,
            v.destnum_length,
            v.destination_id,
            v.country_id,
            v.type_id,
            v.region_id
           FROM ( SELECT 
                    case 
                      when (pr.destnum_length - length($2::text) = 0) then 0
                      when (pr.destnum_length - length($2::text) <> 0 and pr.destnum_length = 0) then 1
                      else 2 end as destnum_length_map,
                    1 AS route_type,
                    pg.providerendpoint_id,
                    pr.provider_ratecard_id,
                    pr.route_type AS route_rule,
                    pr.status,
                    pr.weight,
                    pr.priority,
                    pr.prefix,
                    pr.destnum_length,
                    NULL::integer AS destination_id,
                    NULL::integer AS country_id,
                    NULL::integer AS type_id,
                    NULL::integer AS region_id
                   FROM pyfb_routing_c_routinggrp r
                     JOIN pyfb_routing_prefix_rule pr ON pr.c_route_id = r.routinggroup_id AND pr.status::text <> 'disabled'::text and $2::text LIKE pr.prefix || '%'
                     JOIN pyfb_routing_prefix_rule_provider_gateway_list pg ON pg.prefixrule_id = pr.id
                   where r.customer_id = $1
                UNION ALL
                 SELECT 
                    0 as destnum_length_map,
                    2 AS route_type,
                    deg.providerendpoint_id,
                    der.provider_ratecard_id,
                    der.route_type AS route_rule,
                    der.status,
                    der.weight,
                    der.priority,
                    NULL::character varying AS prefix,
                    0 AS destnum_length,
                    der.destination_id,
                    NULL::integer AS country_id,
                    NULL::integer AS type_id,
                    NULL::integer AS region_id
                   FROM pyfb_routing_c_routinggrp r
                     JOIN pyfb_routing_destination_rule der ON der.c_route_id = r.routinggroup_id AND der.status::text <> 'disabled'::text and der.destination_id = $3
                     JOIN pyfb_routing_destination_rule_provider_gateway_list deg ON deg.destinationrule_id = der.id
                   where r.customer_id = $1
                UNION ALL
                 SELECT 
                    0 as destnum_length_map,
                    3 AS route_type,
                    ctg.providerendpoint_id,
                    ctr.provider_ratecard_id,
                    ctr.route_type AS route_rule,
                    ctr.status,
                    ctr.weight,
                    ctr.priority,
                    NULL::character varying AS prefix,
                    0 AS destnum_length,
                    NULL::integer AS destination_id,
                    ctr.country_id,
                    ctr.type_id,
                    NULL::integer AS region_id
                   FROM pyfb_routing_c_routinggrp r
                     join pyfb_direction_destination pdd on pdd.id = $3
                     join pyfb_direction_country pdc on pdd.country_iso2_id = pdc.country_iso2
                     JOIN pyfb_routing_countrytype_rule ctr ON ctr.c_route_id = r.routinggroup_id AND ctr.status::text <> 'disabled'::text and ctr.country_id = pdc.id and ctr.type_id = pdd.type_id
                     JOIN pyfb_routing_countrytype_rule_provider_gateway_list ctg ON ctg.countrytyperule_id = ctr.id
                   where r.customer_id = $1
                UNION ALL
                 SELECT 
                    0 as destnum_length_map,
                    4 AS route_type,
                    cg.providerendpoint_id,
                    cr.provider_ratecard_id,
                    cr.route_type AS route_rule,
                    cr.status,
                    cr.weight,
                    cr.priority,
                    NULL::character varying AS prefix,
                    0 AS destnum_length,
                    NULL::integer AS destination_id,
                    cr.country_id,
                    NULL::integer AS type_id,
                    NULL::integer AS region_id
                   FROM pyfb_routing_c_routinggrp r
                     join pyfb_direction_destination pdd on pdd.id = $3
                     join pyfb_direction_country pdc on pdd.country_iso2_id = pdc.country_iso2
                     JOIN pyfb_routing_countryrule cr ON cr.c_route_id = r.routinggroup_id AND cr.status::text <> 'disabled'::text and cr.country_id = pdc.id
                     JOIN pyfb_routing_countryrule_provider_gateway_list cg ON cg.countryrule_id = cr.id
                   where r.customer_id = $1
                UNION ALL
                 SELECT 
                    0 as destnum_length_map,
                    5 AS route_type,
                    rtg.providerendpoint_id,
                    rtr.provider_ratecard_id,
                    rtr.route_type AS route_rule,
                    rtr.status,
                    rtr.weight,
                    rtr.priority,
                    NULL::character varying AS prefix,
                    0 AS destnum_length,
                    NULL::integer AS destination_id,
                    NULL::integer AS country_id,
                    rtr.type_id,
                    rtr.region_id
                   FROM pyfb_routing_c_routinggrp r
                     join pyfb_direction_destination pdd on pdd.id = $3
                     join pyfb_direction_country pdc on pdd.country_iso2_id = pdc.country_iso2
                     JOIN pyfb_routing_regiontype_rule rtr ON rtr.c_route_id = r.routinggroup_id AND rtr.status::text <> 'disabled'::text and rtr.region_id = pdc.region_id and rtr.type_id = pdd.type_id
                     JOIN pyfb_routing_regiontype_rule_provider_gateway_list rtg ON rtg.regiontyperule_id = rtr.id
                   where r.customer_id = $1
                UNION ALL
                 SELECT 
                    0 as destnum_length_map,
                    6 AS route_type,
                    rg.providerendpoint_id,
                    rr.provider_ratecard_id,
                    rr.route_type AS route_rule,
                    rr.status,
                    rr.weight,
                    rr.priority,
                    NULL::character varying AS prefix,
                    0 AS destnum_length,
                    NULL::integer AS destination_id,
                    NULL::integer AS country_id,
                    NULL::integer AS type_id,
                    rr.region_id
                   FROM pyfb_routing_c_routinggrp r
                     join pyfb_direction_destination pdd on pdd.id = $3
                     join pyfb_direction_country pdc on pdd.country_iso2_id = pdc.country_iso2
                     JOIN pyfb_routing_region_rule rr ON rr.c_route_id = r.routinggroup_id AND rr.status::text <> 'disabled'::text and rr.region_id = pdc.region_id
                     JOIN pyfb_routing_region_rule_provider_gateway_list rg ON rg.regionrule_id = rr.id
                   where r.customer_id = $1
                union all (
                 SELECT 
                    0 as destnum_length_map,
                    7 AS route_type,
                    dg.providerendpoint_id,
                    dr.provider_ratecard_id,
                    dr.route_type AS route_rule,
                    dr.status,
                    dr.weight,
                    dr.priority,
                    NULL::character varying AS prefix,
                    0 AS destnum_length,
                    NULL::integer AS destination_id,
                    NULL::integer AS country_id,
                    NULL::integer AS type_id,
                    NULL::integer AS region_id
                   FROM pyfb_routing_c_routinggrp r
                     JOIN pyfb_routing_default_rule dr ON dr.c_route_id = r.routinggroup_id AND dr.status::text <> 'disabled'::text
                     JOIN pyfb_routing_default_rule_provider_gateway_list dg ON dg.defaultrule_id = dr.id
                    where r.customer_id = $1) 
                   ) v
                   )
        select 
          r.*, 
            pep."name",
            pep.callee_norm_id,
            pep.callee_norm_in_id,
            pep.caller_id_in_from,
            pep.callerid_norm_id,
            pep.callerid_norm_in_id,
            pep.from_domain,
            pep.pai,
            pep.ppi,
            pep.pid,
            pep.add_plus_in_caller,
            pep.prefix as gwprefix,
            pep.sip_transport,
            pep.sip_port,
            pep.sip_proxy,
            pep.username,
            pep.register as gwregister,
            pep.password,
            pep.realm,
            pep.suffix,
            prpr.estimated_quality,
            prpr.provider_prefix as ratecard_prefix,
            pep.provider_id
        from r 
        join pyfb_endpoint_provider pep on r.providerendpoint_id = pep.id and pep.enabled = true 
        join pyfb_rating_p_ratecard prpr on pep.provider_id = prpr.provider_id and prpr.status = 'enabled'::text AND now() > prpr.date_start AND now() < prpr.date_end
          where route_type = (select min(route_type) from r where destnum_length_map <> 2)
          and case when route_type = 1 then destnum_length_map = (select min(destnum_length_map) from r where route_type = 1) else destnum_length_map = 0 end
          and case when route_type = 1 then r.prefix = (select prefix from r where prefix is not null order by LENGTH(prefix) desc limit 1) else r.prefix is NULL end;
"""


class RouteRepository(BaseRepository):  # noqa: WPS214
    def __init__(self, conn: Connection) -> None:
        super().__init__(conn)

    async def get_outbound_route_list_by_customer_id(
        self,
        *,
        customer_id: int,
        callee_number: str,
        callee_destination_id: int
    ) -> List[RouteSchema]:
        route = {}
        start = time.monotonic()
        route_result = await self._log_and_fetch(
            GET_OUTBOUND_ROUTE_LIST_BY_CUSTOMER_ID_QUERY,
            customer_id,
            callee_number,
            callee_destination_id
        )

        if route_result:
            end = time.monotonic()
            logger.info(f"query timing : {(end-start) * 1000:>8.3f}ms")

        return route_result