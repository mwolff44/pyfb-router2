import time
from typing import List, Optional, Sequence

from asyncpg import Connection, Record

from loguru import logger

from app.db.errors import EntityDoesNotExist
from app.db.repositories.base import BaseRepository
from app.schemas.rate import RateSchema


GET_CUST_RATE_BY_RATECARD_ID_QUERY = """
select * from (SELECT row_number() OVER () AS id,
		v.destnum_length_map,
		v.ratecard_id,
		v.rate_type,
		v.ratecard_name,
		v.rc_type,
		v.status,
		CAST(v.r_rate * 100000 AS INTEGER) AS rate,
		v.r_block_min_duration AS block_min_duration,
		v.r_minimal_time AS minimal_time,
		CAST(v.r_init_block * 100000 AS INTEGER) AS init_block,
		v.prefix,
		v.destnum_length,
		v.destination_id,
		v.country_id,
		v.type_id,
		v.region_id,
    v.rate_id
			FROM ( select * from (select * from (SELECT r.id AS ratecard_id,
            1 AS rate_type,
            case 
              when (pr.destnum_length - length($2::text) = 0) then 0
              when (pr.destnum_length - length($2::text) <> 0 and pr.destnum_length = 0) then 1
              else 2 end as destnum_length_map,
            r.name AS ratecard_name,
            r.rc_type,
            pr.status,
            pr.r_rate,
            pr.r_block_min_duration,
            pr.r_minimal_time,
            pr.r_init_block,
            pr.prefix,
            pr.destnum_length,
            NULL::integer AS destination_id,
            NULL::integer AS country_id,
            NULL::integer AS type_id,
            NULL::integer AS region_id,
            pr.id AS rate_id
           FROM pyfb_rating_c_ratecard r
             JOIN pyfb_rating_c_prefix_rate pr ON pr.c_ratecard_id = r.id AND pr.status::text <> 'disabled'::text and $2::text LIKE pr.prefix || '%'
          WHERE r.status::text = 'enabled'::text AND now() > r.date_start AND now() < r.date_end ) T WHERE destnum_length_map <> 2
          order by destnum_length_map, LENGTH(prefix) desc limit 1) E
        UNION ALL
         SELECT r.id AS ratecard_id,
            2 AS rate_type,
            0 as destnum_length_map,
            r.name AS ratecard_name,
            r.rc_type,
            dr.status,
            dr.r_rate,
            dr.r_block_min_duration,
            dr.r_minimal_time,
            dr.r_init_block,
            NULL::character varying AS prefix,
            0 AS destnum_length,
            dr.destination_id,
            NULL::integer AS country_id,
            NULL::integer AS type_id,
            NULL::integer AS region_id,
            dr.id AS rate_id
           FROM pyfb_rating_c_ratecard r
             JOIN pyfb_rating_c_destination_rate dr ON dr.c_ratecard_id = r.id AND dr.status::text <> 'disabled'::text and dr.destination_id = $3
          WHERE r.status::text = 'enabled'::text AND now() > r.date_start AND now() < r.date_end
        UNION ALL
         SELECT r.id AS ratecard_id,
            3 AS rate_type,
            0 as destnum_length_map,
            r.name AS ratecard_name,
            r.rc_type,
            ctr.status,
            ctr.r_rate,
            ctr.r_block_min_duration,
            ctr.r_minimal_time,
            ctr.r_init_block,
            NULL::character varying AS prefix,
            0 AS destnum_length,
            NULL::integer AS destination_id,
            ctr.country_id,
            ctr.type_id,
            NULL::integer AS region_id,
            ctr.id AS rate_id
           FROM pyfb_rating_c_ratecard r
             join pyfb_direction_destination pdd on pdd.id = $3
						 join pyfb_direction_country pdc on pdd.country_iso2_id = pdc.country_iso2
						 join pyfb_direction_type pdt on pdd.type_id = pdt.id
						 JOIN pyfb_rating_c_countrytype_rate ctr ON ctr.c_ratecard_id = r.id AND ctr.status::text <> 'disabled'::text
						   and ctr.country_id = pdc.id and ctr.type_id = pdt.id
          WHERE r.status::text = 'enabled'::text AND now() > r.date_start AND now() < r.date_end
        UNION ALL
         SELECT r.id AS ratecard_id,
            4 AS rate_type,
            0 as destnum_length_map,
            r.name AS ratecard_name,
            r.rc_type,
            cr.status,
            cr.r_rate,
            cr.r_block_min_duration,
            cr.r_minimal_time,
            cr.r_init_block,
            NULL::character varying AS prefix,
            0 AS destnum_length,
            NULL::integer AS destination_id,
            cr.country_id,
            NULL::integer AS type_id,
            NULL::integer AS region_id,
            cr.id AS rate_id
           FROM pyfb_rating_c_ratecard r
             join pyfb_direction_destination pdd on pdd.id = $3
             join pyfb_direction_country pdc on pdd.country_iso2_id = pdc.country_iso2
						 JOIN pyfb_rating_c_country_rate cr ON cr.c_ratecard_id = r.id AND cr.status::text <> 'disabled'::text
						   and cr.country_id = pdc.id
          WHERE r.status::text = 'enabled'::text AND now() > r.date_start AND now() < r.date_end
        UNION ALL
         SELECT r.id AS ratecard_id,
            5 AS rate_type,
            0 as destnum_length_map,
            r.name AS ratecard_name,
            r.rc_type,
            rtr.status,
            rtr.r_rate,
            rtr.r_block_min_duration,
            rtr.r_minimal_time,
            rtr.r_init_block,
            NULL::character varying AS prefix,
            0 AS destnum_length,
            NULL::integer AS destination_id,
            NULL::integer AS country_id,
            rtr.type_id,
            rtr.region_id,
            rtr.id AS rate_id
           FROM pyfb_rating_c_ratecard r
             join pyfb_direction_destination pdd on pdd.id = $3
						 join pyfb_direction_country pdc on pdd.country_iso2_id = pdc.country_iso2
						 join pyfb_direction_region pdr on pdc.region_id = pdr.id
             join pyfb_direction_type pdt on pdd.type_id = pdt.id
						 JOIN pyfb_rating_c_regiontype_rate rtr ON rtr.c_ratecard_id = r.id AND rtr.status::text <> 'disabled'::text
						   and rtr.region_id = pdr.id and rtr.type_id = pdt.id
          WHERE r.status::text = 'enabled'::text AND now() > r.date_start AND now() < r.date_end
        UNION ALL
         SELECT r.id AS ratecard_id,
            6 AS rate_type,
            0 as destnum_length_map,
            r.name AS ratecard_name,
            r.rc_type,
            rr.status,
            rr.r_rate,
            rr.r_block_min_duration,
            rr.r_minimal_time,
            rr.r_init_block,
            NULL::character varying AS prefix,
            0 AS destnum_length,
            NULL::integer AS destination_id,
            NULL::integer AS country_id,
            NULL::integer AS type_id,
            rr.region_id,
            rr.id AS rate_id
           FROM pyfb_rating_c_ratecard r
             join pyfb_direction_destination pdd on pdd.id = $3
						 join pyfb_direction_country pdc on pdd.country_iso2_id = pdc.country_iso2
						 join pyfb_direction_region pdr on pdc.region_id = pdr.id
						 JOIN pyfb_rating_c_region_rate rr ON rr.c_ratecard_id = r.id AND rr.status::text <> 'disabled'::text
						   and rr.region_id = pdr.id
          WHERE r.status::text = 'enabled'::text AND now() > r.date_start AND now() < r.date_end
        UNION ALL
         SELECT r.id AS ratecard_id,
            7 AS rate_type,
            0 as destnum_length_map,
            r.name AS ratecard_name,
            r.rc_type,
            dr.status,
            dr.r_rate,
            dr.r_block_min_duration,
            dr.r_minimal_time,
            dr.r_init_block,
            NULL::character varying AS prefix,
            0 AS destnum_length,
            NULL::integer AS destination_id,
            NULL::integer AS country_id,
            NULL::integer AS type_id,
            NULL::integer AS region_id,
            dr.id AS rate_id
           FROM pyfb_rating_c_ratecard r
             JOIN pyfb_rating_c_default_rate dr ON dr.c_ratecard_id = r.id AND dr.status::text <> 'disabled'::text
          WHERE r.status::text = 'enabled'::text AND now() > r.date_start AND now() < r.date_end) v)
          custrate where ratecard_id = $1 order by rate_type limit 1;
"""

GET_PROV_RATE_BY_RATECARD_ID_QUERY = """
select * from (SELECT row_number() OVER () AS id,
		v.destnum_length_map,
		v.ratecard_id,
		v.rate_type,
		v.ratecard_name,
		v.rc_type,
		v.status,
		CAST(v.r_rate * 100000 AS INTEGER) AS rate,
		v.r_block_min_duration,
		v.r_minimal_time,
		CAST(v.r_init_block* 100000 AS INTEGER) AS init_block,
		v.prefix,
		v.destnum_length,
		v.destination_id,
		v.country_id,
		v.type_id,
		v.region_id,
    v.rate_id
	FROM ( select * from (select * from (SELECT r.provider_id,
            r.id AS ratecard_id,
            case 
              when (pr.destnum_length - length($2::text) = 0) then 0
              when (pr.destnum_length - length($2::text) <> 0 and pr.destnum_length = 0) then 1
              else 2 end as destnum_length_map,
            1 AS rate_type,
            r.name AS ratecard_name,
            r.provider_prefix,
            r.estimated_quality,
            r.rc_type,
            pr.status,
            pr.r_rate,
            pr.r_block_min_duration,
            pr.r_minimal_time,
            pr.r_init_block,
            pr.prefix,
            pr.destnum_length,
            NULL::integer AS destination_id,
            NULL::integer AS country_id,
            NULL::integer AS type_id,
            NULL::integer AS region_id,
            pr.id AS rate_id
           FROM pyfb_rating_p_ratecard r
             JOIN pyfb_rating_p_prefix_rate pr ON pr.p_ratecard_id = r.id AND pr.status::text <> 'disabled'::text and $2::text LIKE pr.prefix || '%'
          WHERE r.status::text = 'enabled'::text AND now() > r.date_start AND now() < r.date_end ) T WHERE destnum_length_map <> 2
          order by destnum_length_map, LENGTH(prefix) desc limit 1) E
        UNION ALL
         SELECT r.provider_id,
            r.id AS ratecard_id,
            0 as destnum_length_map,
            2 AS rate_type,
            r.name AS ratecard_name,
            r.provider_prefix,
            r.estimated_quality,
            r.rc_type,
            dr.status,
            dr.r_rate,
            dr.r_block_min_duration,
            dr.r_minimal_time,
            dr.r_init_block,
            NULL::character varying AS prefix,
            0 AS destnum_length,
            dr.destination_id,
            NULL::integer AS country_id,
            NULL::integer AS type_id,
            NULL::integer AS region_id,
            dr.id AS rate_id
           FROM pyfb_rating_p_ratecard r
             JOIN pyfb_rating_p_destination_rate dr ON dr.p_ratecard_id = r.id AND dr.status::text <> 'disabled'::text and dr.destination_id = $3
          WHERE r.status::text = 'enabled'::text AND now() > r.date_start AND now() < r.date_end
        UNION ALL
         SELECT r.provider_id,
            r.id AS ratecard_id,
            0 as destnum_length_map,
            3 AS rate_type,
            r.name AS ratecard_name,
            r.provider_prefix,
            r.estimated_quality,
            r.rc_type,
            ctr.status,
            ctr.r_rate,
            ctr.r_block_min_duration,
            ctr.r_minimal_time,
            ctr.r_init_block,
            NULL::character varying AS prefix,
            0 AS destnum_length,
            NULL::integer AS destination_id,
            ctr.country_id,
            ctr.type_id,
            NULL::integer AS region_id,
            ctr.id AS rate_id
           FROM pyfb_rating_p_ratecard r
             join pyfb_direction_destination pdd on pdd.id = $3
						 join pyfb_direction_country pdc on pdd.country_iso2_id = pdc.country_iso2
						 join pyfb_direction_type pdt on pdd.type_id = pdt.id
						 JOIN pyfb_rating_p_countrytype_rate ctr ON ctr.p_ratecard_id = r.id AND ctr.status::text <> 'disabled'::text
						   and ctr.country_id = pdc.id and ctr.type_id = pdt.id
          WHERE r.status::text = 'enabled'::text AND now() > r.date_start AND now() < r.date_end
        UNION ALL
         SELECT r.provider_id,
            r.id AS ratecard_id,
            0 as destnum_length_map,
            4 AS rate_type,
            r.name AS ratecard_name,
            r.provider_prefix,
            r.estimated_quality,
            r.rc_type,
            cr.status,
            cr.r_rate,
            cr.r_block_min_duration,
            cr.r_minimal_time,
            cr.r_init_block,
            NULL::character varying AS prefix,
            0 AS destnum_length,
            NULL::integer AS destination_id,
            cr.country_id,
            NULL::integer AS type_id,
            NULL::integer AS region_id,
            cr.id AS rate_id
           FROM pyfb_rating_p_ratecard r
             join pyfb_direction_destination pdd on pdd.id = $3
             join pyfb_direction_country pdc on pdd.country_iso2_id = pdc.country_iso2
						 JOIN pyfb_rating_p_country_rate cr ON cr.p_ratecard_id = r.id AND cr.status::text <> 'disabled'::text
						   and cr.country_id = pdc.id
          WHERE r.status::text = 'enabled'::text AND now() > r.date_start AND now() < r.date_end
        UNION ALL
         SELECT r.provider_id,
            r.id AS ratecard_id,
            0 as destnum_length_map,
            5 AS rate_type,
            r.name AS ratecard_name,
            r.provider_prefix,
            r.estimated_quality,
            r.rc_type,
            rtr.status,
            rtr.r_rate,
            rtr.r_block_min_duration,
            rtr.r_minimal_time,
            rtr.r_init_block,
            NULL::character varying AS prefix,
            0 AS destnum_length,
            NULL::integer AS destination_id,
            NULL::integer AS country_id,
            rtr.type_id,
            rtr.region_id,
            rtr.id AS rate_id
           FROM pyfb_rating_p_ratecard r
             join pyfb_direction_destination pdd on pdd.id = $3
						 join pyfb_direction_country pdc on pdd.country_iso2_id = pdc.country_iso2
						 join pyfb_direction_region pdr on pdc.region_id = pdr.id
             join pyfb_direction_type pdt on pdd.type_id = pdt.id
						 JOIN pyfb_rating_p_regiontype_rate rtr ON rtr.p_ratecard_id = r.id AND rtr.status::text <> 'disabled'::text
						   and rtr.region_id = pdr.id and rtr.type_id = pdt.id
          WHERE r.status::text = 'enabled'::text AND now() > r.date_start AND now() < r.date_end
        UNION ALL
         SELECT r.provider_id,
            r.id AS ratecard_id,
            0 as destnum_length_map,
            6 AS rate_type,
            r.name AS ratecard_name,
            r.provider_prefix,
            r.estimated_quality,
            r.rc_type,
            rr.status,
            rr.r_rate,
            rr.r_block_min_duration,
            rr.r_minimal_time,
            rr.r_init_block,
            NULL::character varying AS prefix,
            0 AS destnum_length,
            NULL::integer AS destination_id,
            NULL::integer AS country_id,
            NULL::integer AS type_id,
            rr.region_id,
            rr.id AS rate_id
           FROM pyfb_rating_p_ratecard r
             join pyfb_direction_destination pdd on pdd.id = $3
						 join pyfb_direction_country pdc on pdd.country_iso2_id = pdc.country_iso2
						 join pyfb_direction_region pdr on pdc.region_id = pdr.id
						 JOIN pyfb_rating_p_region_rate rr ON rr.p_ratecard_id = r.id AND rr.status::text <> 'disabled'::text
						   and rr.region_id = pdr.id
          WHERE r.status::text = 'enabled'::text AND now() > r.date_start AND now() < r.date_end
        UNION ALL
         SELECT r.provider_id,
            r.id AS ratecard_id,
            0 as destnum_length_map,
            7 AS rate_type,
            r.name AS ratecard_name,
            r.provider_prefix,
            r.estimated_quality,
            r.rc_type,
            dr.status,
            dr.r_rate,
            dr.r_block_min_duration,
            dr.r_minimal_time,
            dr.r_init_block,
            NULL::character varying AS prefix,
            0 AS destnum_length,
            NULL::integer AS destination_id,
            NULL::integer AS country_id,
            NULL::integer AS type_id,
            NULL::integer AS region_id,
            dr.id AS rate_id
           FROM pyfb_rating_p_ratecard r
             JOIN pyfb_rating_p_default_rate dr ON dr.p_ratecard_id = r.id AND dr.status::text <> 'disabled'::text
          WHERE r.status::text = 'enabled'::text AND now() > r.date_start AND now() < r.date_end) v)
  provrate where ratecard_id = $1 order by rate_type limit 1;
"""

class RateRepository(BaseRepository):  # noqa: WPS214
    def __init__(self, conn: Connection) -> None:
        super().__init__(conn)

    async def get_cust_rate_by_ratecard_id(
        self,
        *,
        ratecard_id: int,
        callee_number: str,
        callee_destination_id: int
    ) -> RateSchema:

        rate = {}
        start = time.monotonic()

        rate = await self._log_and_fetch_row(
            GET_CUST_RATE_BY_RATECARD_ID_QUERY, 
            ratecard_id,
            callee_number,
            callee_destination_id
        )

        if rate:
            end = time.monotonic()
            logger.info(f"query timing : {(end-start) * 1000:>8.3f}ms")

        return rate


    async def get_prov_rate_by_ratecard_id(
        self,
        *,
        ratecard_id: int,
        callee_number: str,
        callee_destination_id: int
    ) -> RateSchema:

        provrate = {}
        start = time.monotonic()

        provrate = await self._log_and_fetch_row(
            GET_PROV_RATE_BY_RATECARD_ID_QUERY, 
            ratecard_id,
            callee_number,
            callee_destination_id
        )

        if provrate:
            end = time.monotonic()
            logger.info(f"query timing : {(end-start) * 1000:>8.3f}ms")

        return provrate
