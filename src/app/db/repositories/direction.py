import time

from typing import List, Optional, Sequence

from asyncpg import Connection, Record

from loguru import logger

from app.db.errors import EntityDoesNotExist
from app.db.repositories.base import BaseRepository
from app.schemas.direction import DirectionSchema


GET_DIRECTION_BY_CALL_NUMBER_QUERY = """
select p.id as prefix_id, d.id, d.name
		from pyfb_direction_destination d
		join pyfb_direction_prefix p on d.id = p.destination_id
		AND $1 LIKE p.prefix || '%'
		order by length(p.prefix) desc limit 1;
"""


class DirectionRepository(BaseRepository):  # noqa: WPS214
    def __init__(self, conn: Connection) -> None:
        super().__init__(conn)

    async def get_direction_by_call_number(self, *, phonenumber: str) -> DirectionSchema:
        direction = {}
        start = time.monotonic()
        direction_result = await self._log_and_fetch_row(
            GET_DIRECTION_BY_CALL_NUMBER_QUERY, phonenumber
        )

        if direction_result:
            end = time.monotonic()
            logger.info(f"query timing : {(end-start) * 1000:>8.3f}ms")

            direction["prefix_id"] =  direction_result["prefix_id"]
            direction["destination_id"] =  direction_result["id"]
            direction["destination_name"] =  direction_result["name"]

            return direction

        raise EntityDoesNotExist("direction with phone number {0} does not exist".format(phonenumber))