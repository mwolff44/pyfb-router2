import time

from typing import List, Optional, Sequence

from asyncpg import Connection, Record

from loguru import logger

from app.db.errors import EntityDoesNotExist
from app.db.repositories.base import BaseRepository
from app.schemas.ratecard import RatecardSchema


GET_RATECARD_LIST_BY_CUSTOMER_ID_QUERY = """
with r as ( SELECT 
            cra.tech_prefix,
            cra.priority,
            cra.discount,
            cra.allow_negative_margin,
            cra.ratecard_id,
            c.name,
            c.callerid_list_id,
            prcl.callerid_filter, -- 2 authorized 3 unauthorized
            prcld.destination_id
            FROM pyfb_rating_cr_allocation cra
              join pyfb_rating_c_ratecard c on c.id = cra.ratecard_id
                left join pyfb_rating_callernum_list prcl on prcl.id = c.callerid_list_id
                left join pyfb_rating_callernum_list_destination prcld on prcld.callernumlist_id = prcl.id and prcld.destination_id = $2
            WHERE c.status = 'enabled' AND cra.customer_id = $1 and c.rc_type = 'pstn' AND now() > c.date_start AND now() < c.date_end) 
        SELECT tech_prefix,
          priority,
          discount,
          allow_negative_margin,
          ratecard_id 
        FROM r WHERE r.callerid_list_id is null or 
        case when (r.callerid_list_id is not null and destination_id is null) then callerid_filter = '3' else case when (r.callerid_list_id is not null and destination_id is not null) then callerid_filter = '2' end end
        order by priority ASC;
"""


class RatecardRepository(BaseRepository):  # noqa: WPS214
    def __init__(self, conn: Connection) -> None:
        super().__init__(conn)

    async def get_ratecard_list_by_customer_id(self, *, customer_id: int, caller_destination_id: int) -> List[RatecardSchema]:
        ratecard = {}
        start = time.monotonic()
        ratecard_result = await self._log_and_fetch(
            GET_RATECARD_LIST_BY_CUSTOMER_ID_QUERY,
            customer_id,
            caller_destination_id
        )

        if ratecard_result:
            end = time.monotonic()
            logger.info(f"query timing : {(end-start) * 1000:>8.3f}ms")

        return ratecard_result