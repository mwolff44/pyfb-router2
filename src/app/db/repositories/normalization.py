import time

from typing import List, Optional, Sequence

from asyncpg import Connection, Record

from loguru import logger

from app.db.errors import EntityDoesNotExist
from app.db.repositories.base import BaseRepository
from app.schemas.normalization import NormalizationSchema


GET_NORM_RULE_LIST_BY_GROUP_ID_QUERY = """
SELECT ng.name AS profile, nr.name AS rule, nrg.pr, nr.match_len, nr.match_op, nr.match_exp, nr.subst_exp, nr.repl_exp 
    FROM pyfb_normalization_rule_grp nrg 
    JOIN pyfb_normalization_rule nr ON nr.id = nrg.rule_id 
    JOIN pyfb_normalization_grp ng on ng.id = nrg.dpid_id 
    WHERE nrg.dpid_id = $1 
    ORDER BY nr.match_len DESC, nrg.pr ASC;
"""


class NormalizationRepository(BaseRepository):  # noqa: WPS214
    def __init__(self, conn: Connection) -> None:
        super().__init__(conn)

    async def get_norm_rule_list_by_group_id(self, *, group_id: int) -> List[NormalizationSchema]:
        normalization = {}
        start = time.monotonic()
        normalization_result = await self._log_and_fetch(
            GET_NORM_RULE_LIST_BY_GROUP_ID_QUERY,
            group_id
        )

        if normalization_result:
            end = time.monotonic()
            logger.info(f"query timing : {(end-start) * 1000:>8.3f}ms")

        return normalization_result