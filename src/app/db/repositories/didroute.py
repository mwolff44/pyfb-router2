import time

from typing import List, Optional, Sequence

from asyncpg import Connection, Record

from loguru import logger

from app.db.errors import EntityDoesNotExist
from app.db.repositories.base import BaseRepository
from app.schemas.route import DidRouteSchema


GET_OUTBOUND_ROUTE_LIST_BY_CUSTOMER_ID_QUERY = """
SELECT d.number AS did,
    d.customer_id,
    d.provider_id,
    pdr.order,
    pdr.type AS dst_type,
    pdr.number AS dst_number,
    pdr.weight,
    pdr.trunk_id,
    pec.name,
    pec.pai,
    pec.pid,
    pec.ppi,
    pec.callee_norm_in_id,
    pec.callerid_norm_in_id,
    l.contact,
    l.received,
    l.path, l.q,
    l.expires,
    pec.registration,
    pec.sip_ip,
    pec.sip_port,
    pec.sip_transport
FROM pyfb_did d 
JOIN pyfb_did_routes pdr ON pdr.contract_did_id = d.id
LEFT JOIN pyfb_endpoint_customer pec ON pec.id = pdr.trunk_id
LEFT JOIN location l ON l.username = pec.name
WHERE $1 ~* (d.number || '$')
ORDER BY pdr.order ASC, l.q DESC;
"""


class DidRouteRepository(BaseRepository):  # noqa: WPS214
    def __init__(self, conn: Connection) -> None:
        super().__init__(conn)

    async def get_did_route_list_by_did(
        self,
        *,
        did: str
    ) -> List[DidRouteSchema]:
        route = {}
        start = time.monotonic()
        route_result = await self._log_and_fetch(
            GET_OUTBOUND_ROUTE_LIST_BY_CUSTOMER_ID_QUERY,
            did
        )

        if route_result:
            end = time.monotonic()
            logger.info(f"query timing : {(end-start) * 1000:>8.3f}ms")

        return route_result
