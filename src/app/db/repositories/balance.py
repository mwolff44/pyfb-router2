import time

from typing import List, Optional, Sequence

from asyncpg import Connection, Record

from loguru import logger

from app.db.errors import EntityDoesNotExist
from app.db.repositories.base import BaseRepository
from app.schemas.balance import BalanceSchema


GET_BALANCE_STATUS_BY_CUSTOMER_ID_QUERY = """
select pyfb_company.customer_balance, pyfb_customer.credit_limit, pyfb_customer.customer_enabled, pyfb_customer.blocking_credit_limit
        from pyfb_company
        join pyfb_customer on pyfb_customer.company_id = pyfb_company.id
        where pyfb_customer.id=$1
"""


class BalanceRepository(BaseRepository):  # noqa: WPS214
    def __init__(self, conn: Connection) -> None:
        super().__init__(conn)

    async def get_balance_status_by_customer_id(self, *, id: int) -> BalanceSchema:
        balance = {}
        start = time.monotonic()
        balance_result = await self._log_and_fetch_row(
            GET_BALANCE_STATUS_BY_CUSTOMER_ID_QUERY, id
        )

        if balance_result:
            end = time.monotonic()
            logger.info(f"query timing : {(end-start) * 1000:>8.3f}ms")
            
            if (balance_result["customer_balance"] > balance_result["credit_limit"]):
                balance["balance_status"] = True
            else:
                balance["balance_status"] = False
            balance["customer_status"] = balance_result["customer_enabled"]
            balance["blocking_credit_limit"] = balance_result["blocking_credit_limit"]

            return balance

        raise EntityDoesNotExist("balance with customer {0} does not exist".format(id))