import pytest
from fastapi import FastAPI
from httpx import AsyncClient
from starlette import status


pytestmark = pytest.mark.asyncio


async def test_ping(
    app: FastAPI, client: AsyncClient
) -> None:
    response = await client.get(app.url_path_for("status:ping"))
    assert response.status_code == 200
    assert response.json() == {"ping": "pong!"}
