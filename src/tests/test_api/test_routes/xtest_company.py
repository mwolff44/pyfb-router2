""" import json

import pytest

from app.services import company


def test_create_company(test_app, monkeypatch):
    test_request_payload = {"name": "something", "customer_balance": 151.001, "supplier_balance": 11.0012}
    test_response_payload = {"id": 1, "name": "something", "customer_balance": 151.001, "supplier_balance": 11.0012}

    async def mock_post(payload):
        return 1

    monkeypatch.setattr(company, "post", mock_post)

    response = test_app.post("/company/", data=json.dumps(test_request_payload),)

    assert response.status_code == 201
    assert response.json() == test_response_payload


def test_create_company_invalid_json(test_app):
    response = test_app.post("/company/", data=json.dumps({"name": "so"}))
    assert response.status_code == 422

    response = test_app.post("/company/", data=json.dumps({"name": "szdzdz1", "customer_balance": "x"}))
    assert response.status_code == 422


def test_read_company(test_app, monkeypatch):
    test_data = {"id": 1, "name": "something", "customer_balance": 151.001, "supplier_balance": 11.0012}

    async def mock_get(id):
        return test_data

    monkeypatch.setattr(company, "get", mock_get)

    response = test_app.get("/company/1")
    assert response.status_code == 200
    assert response.json() == test_data


def test_read_company_incorrect_id(test_app, monkeypatch):
    async def mock_get(id):
        return None

    monkeypatch.setattr(company, "get", mock_get)

    response = test_app.get("/company/999")
    assert response.status_code == 404
    assert response.json()["detail"] == "company not found"

    response = test_app.get("/company/0")
    assert response.status_code == 422


def test_read_all_company(test_app, monkeypatch):
    test_data = [
        {"name": "something", "customer_balance": 151.001, "supplier_balance": 11.0012, "id": 1},
        {"name": "someone", "customer_balance": 251.001, "supplier_balance": 1.0012, "id": 2},
    ]

    async def mock_get_all():
        return test_data

    monkeypatch.setattr(company, "get_all", mock_get_all)

    response = test_app.get("/company/")
    assert response.status_code == 200
    assert response.json() == test_data


def test_update_company(test_app, monkeypatch):
    test_update_data = {"name": "someone", "customer_balance": 151.101, "supplier_balance": 11.1012, "id": 1}

    async def mock_get(id):
        return True

    monkeypatch.setattr(company, "get", mock_get)

    async def mock_put(id, payload):
        return 1

    monkeypatch.setattr(company, "put", mock_put)

    response = test_app.put("/company/1/", data=json.dumps(test_update_data))
    assert response.status_code == 200
    assert response.json() == test_update_data


@pytest.mark.parametrize(
    "id, payload, status_code",
    [
        [1, {}, 422],
        [1, {"customer_balance": "x"}, 422],
        [999, {"name": "foovfvf"}, 404],
        [1, {"name": "1", "customer_balance": 151.001, "supplier_balance": "x"}, 422],
        [1, {"name": "foo1", "customer_balance": "x", "supplier_balance": 11.0012}, 422],
        [0, {"name": "foo", "customer_balance": 51.001, "supplier_balance": 11.0012}, 422],
    ],
)
def test_update_company_invalid(test_app, monkeypatch, id, payload, status_code):
    async def mock_get(id):
        return None

    monkeypatch.setattr(company, "get", mock_get)

    response = test_app.put(f"/company/{id}/", data=json.dumps(payload),)
    assert response.status_code == status_code



def test_remove_company(test_app, monkeypatch):
    test_data = {"name": "something", "customer_balance": 151.001, "supplier_balance": 11.0012, "id": 1}

    async def mock_get(id):
        return test_data

    monkeypatch.setattr(company, "get", mock_get)

    async def mock_delete(id):
        return id

    monkeypatch.setattr(company, "delete", mock_delete)

    response = test_app.delete("/company/1/")
    assert response.status_code == 200
    assert response.json() == test_data


def test_remove_company_incorrect_id(test_app, monkeypatch):
    async def mock_get(id):
        return None

    monkeypatch.setattr(company, "get", mock_get)

    response = test_app.delete("/company/999/")
    assert response.status_code == 404
    assert response.json()["detail"] == "company not found"

    response = test_app.delete("/company/0/")
    assert response.status_code == 422 """