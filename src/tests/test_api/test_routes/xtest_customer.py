""" import json

import pytest

from app.services import customer


def test_create_customer(test_app, monkeypatch):
    test_request_payload = {"credit_limit": 151.001, "low_credit_alert": 10, "max_calls": 10, "calls_per_second" : 11, "customer_enabled": True, "company_id": 1}
    test_response_payload = {"id": 1, "credit_limit": 151.001, "low_credit_alert": 10, "max_calls": 10, "calls_per_second" : 11, "customer_enabled": True, "company_id": 1}

    async def mock_post(payload):
        return 1

    monkeypatch.setattr(customer, "post", mock_post)

    response = test_app.post("/customer/", data=json.dumps(test_request_payload),)

    assert response.status_code == 201
    assert response.json() == test_response_payload


def test_create_customer_invalid_json(test_app):
    response = test_app.post("/customer/", data=json.dumps({"credit_limit": "so"}))
    assert response.status_code == 422

    response = test_app.post("/customer/", data=json.dumps({"low_credit_alert": "szdzdz1"}))
    assert response.status_code == 422


def test_read_customer(test_app, monkeypatch):
    test_data = {"id": 1, "credit_limit": 151.001, "low_credit_alert": 10, "max_calls": 10, "calls_per_second" : 11, "customer_enabled": True, "company_id": 1}

    async def mock_get(id):
        return test_data

    monkeypatch.setattr(customer, "get", mock_get)

    response = test_app.get("/customer/1")
    assert response.status_code == 200
    assert response.json() == test_data


def test_read_customer_incorrect_id(test_app, monkeypatch):
    async def mock_get(id):
        return None

    monkeypatch.setattr(customer, "get", mock_get)

    response = test_app.get("/customer/999")
    assert response.status_code == 404
    assert response.json()["detail"] == "customer not found"

    response = test_app.get("/customer/0")
    assert response.status_code == 422


def test_read_all_customer(test_app, monkeypatch):
    test_data = [
        {"credit_limit": 151.001, "low_credit_alert": 10, "max_calls": 10, "calls_per_second" : 11, "customer_enabled": True, "company_id": 1, "id": 1},
        {"credit_limit": 51.001, "low_credit_alert": 0, "max_calls": 1, "calls_per_second" : 1, "customer_enabled": False, "company_id": 2, "id": 2},
    ]

    async def mock_get_all():
        return test_data

    monkeypatch.setattr(customer, "get_all", mock_get_all)

    response = test_app.get("/customer/")
    assert response.status_code == 200
    assert response.json() == test_data


def test_update_customer(test_app, monkeypatch):
    test_update_data = {"credit_limit": 151.001, "low_credit_alert": 10, "max_calls": 10, "calls_per_second" : 10, "customer_enabled": True, "company_id": 1, "id": 1}

    async def mock_get(id):
        return True

    monkeypatch.setattr(customer, "get", mock_get)

    async def mock_put(id, payload):
        return 1

    monkeypatch.setattr(customer, "put", mock_put)

    response = test_app.put("/customer/1/", data=json.dumps(test_update_data))
    assert response.status_code == 200
    assert response.json() == test_update_data


@pytest.mark.parametrize(
    "id, payload, status_code",
    [
        [1, {}, 422],
        [1, {"credit_limit": "x"}, 422],
        [999, {"company_id": 1}, 404],
        [1, {"credit_limit": 151.001, "low_credit_alert": 10, "max_calls": -1, "calls_per_second" : 11, "customer_enabled": True, "company_id": 1}, 422],
        [1, {"credit_limit": 151.001, "low_credit_alert": 10, "max_calls": 10, "calls_per_second" : 11, "customer_enabled": True, "company_id": "x"}, 422],
        [0, {"credit_limit": 151.001, "low_credit_alert": 10, "max_calls": 10, "calls_per_second" : 11, "customer_enabled": True, "company_id": 1}, 422],
    ],
)
def test_update_customer_invalid(test_app, monkeypatch, id, payload, status_code):
    async def mock_get(id):
        return None

    monkeypatch.setattr(customer, "get", mock_get)

    response = test_app.put(f"/customer/{id}/", data=json.dumps(payload),)
    assert response.status_code == status_code



def test_remove_customer(test_app, monkeypatch):
    test_data = {"credit_limit": 151.001, "low_credit_alert": 10, "max_calls": 10, "calls_per_second" : 11, "customer_enabled": True, "company_id": 1, "id": 1}

    async def mock_get(id):
        return test_data

    monkeypatch.setattr(customer, "get", mock_get)

    async def mock_delete(id):
        return id

    monkeypatch.setattr(customer, "delete", mock_delete)

    response = test_app.delete("/customer/1/")
    assert response.status_code == 200
    assert response.json() == test_data


def test_remove_customer_incorrect_id(test_app, monkeypatch):
    async def mock_get(id):
        return None

    monkeypatch.setattr(customer, "get", mock_get)

    response = test_app.delete("/customer/999/")
    assert response.status_code == 404
    assert response.json()["detail"] == "customer not found"

    response = test_app.delete("/customer/0/")
    assert response.status_code == 422 """