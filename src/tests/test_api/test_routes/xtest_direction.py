""" import asyncio

from sqlalchemy.sql import select

import json

import pytest

from app.db import database
from app.models import t_customer, t_company
from app.services import direction

import nest_asyncio


@pytest.mark.asyncio
async def test_read_direction(test_app, monkeypatch):

    test_response = {
        "prefix_id": 1,
        "destination_id": 1,
        "destination_name": "FRANCE MOBILE"
    }


    @pytest.mark.asyncio
    async def create_destination():
        query1 = """DELETE FROM pyfb_direction_prefix"""
        query2 = """DELETE FROM pyfb_direction_destination"""
        query3 = """INSERT INTO pyfb_direction_destination(id, name) VALUES (:id, :name)"""
        query4 = """INSERT INTO pyfb_direction_prefix(id, prefix, destination_id) VALUES (:id, :prefix, :destination_id)"""
        values_destination = [
            {"id": 1, "name": "FRANCE MOBILE"},
        ]
        values_prefix = [
            {"id": 1, "prefix": "336", "destination_id": 1},
        ]

        await database.connect()
        await database.execute(query=query1)
        await database.execute(query=query2)
        await database.execute_many(query=query3, values=values_destination)
        await database.execute_many(query=query4, values=values_prefix)

        query = """SELECT * FROM pyfb_direction_prefix"""
        rows = await database.fetch_all(query=query)
        print(len(rows))
        print(dict(rows[0]))

        query = """SELECT * FROM pyfb_direction_destination"""
        rows = await database.fetch_all(query=query)
        print(len(rows))
        print(dict(rows[0]))

        await database.disconnect()

        return

    #async def mock_get(id):
    #    return test_response

    #monkeypatch.setattr(direction, "get", mock_get)

    nest_asyncio.apply()
    await database.connect()
    #dest = await create_destination()
    #import pdb; pdb.set_trace()
    phonenumber = str(336)


    response = test_app.get("/direction/" + str(phonenumber))
    await database.disconnect()
    
    assert response.status_code == 200
    assert response.json() == test_response


@pytest.mark.asyncio
async def test_read_direction_incorrect_prefix(test_app, monkeypatch):
    async def mock_get(phonenumber):
        return None

    monkeypatch.setattr(direction, "get", mock_get)

    response = test_app.get("/direction/999")

    assert response.status_code == 404
    assert response.json()["detail"] == "Direction not found"

 """