import pytest

from asyncpg.pool import Pool
from fastapi import FastAPI
from httpx import AsyncClient
from starlette import status

from app.db.errors import EntityDoesNotExist
from app.db.repositories.balance import BalanceRepository

pytestmark = pytest.mark.asyncio


async def test_read_balance(
    app: FastAPI,
    client: AsyncClient
):

    test_data = {
        "customer_id": 1,
        "customer_balance": 12.6,
        "credit_limit": 10.01,
        "customer_enabled": True
    }
    test_response = {
        "balance_status": True,
        "customer_status": True
    }
    """    comp_items = [
        {
            'id' : 1,
            'name': 'celea',
            'customer_balance': '100.02'
        },
        {
            'id' : 2,
            'name': 'no_money',
            'customer_balance': '0'

        },
        {
            'id' : 3,
            'name': 'owe_money',
            'customer_balance': '-10.05'

        },
        {
            'id' : 4,
            'name': 'ccust_disabled',
            'customer_balance': '50.004'

        }
    ]
    cust_items = [
        {
            'id': 1,
            'credit_limit': '0',
            'customer_enabled': True,
            'company_id': 1
        },
        {
            'id': 2,
            'credit_limit': '0',
            'customer_enabled': True,
            'company_id': 2
        },
        {
            'id': 3,
            'credit_limit': '-2',
            'customer_enabled': True,
            'company_id': 3
        },
        {
            'id': 4,
            'credit_limit': '10',
            'customer_enabled': False,
            'company_id': 4
        }
    ]
    @pytest.mark.asyncio
    async def create_company():
        query1 = '"""'DELETE FROM pyfb_customer'"""'
        query2 = '"""'DELETE FROM pyfb_company'"""'

        await database.connect()
        await database.execute(query=query1)
        await database.execute(query=query2)
        q_company = t_company.insert().values(
            name="CELEA7",
            customer_balance=100,
            supplier_balance=0
        )
        company = await database.execute(query=q_company)
        print(company)

        q_customer = t_customer.insert().values(
            credit_limit=0,
            customer_enabled=True,
            low_credit_alert=0,
            max_calls=10,
            calls_per_second=1,
            company_id=company
        )
        customer = await database.execute(query=q_customer)
        print(customer)
        
        await database.disconnect()

        return company """


    # customer = await create_company()
    customer = 51

    response = await client.get(
        app.url_path_for(
            "balance:get",
            id=1
        )
    )
    
    assert response.status_code == 200
    assert response.json() == test_response


async def test_read_balance_incorrect_id(
    app: FastAPI, client: AsyncClient
) -> None:

    response = await client.get(
        app.url_path_for(
            "balance:get",
            id=999
        )
    )
    assert response.status_code == 404
    assert response.json()["detail"] == "Customer not found"

    response = await client.get(
        app.url_path_for(
            "balance:get",
            id=0
        )
    )
    assert response.status_code == 422
